// Sriramajayam

#include "SimoShellBase.h"

using namespace SimoShell;

// Nodal positions 
std::vector< std::vector<double> >* SimoShellBase::Xn = NULL;
  
// Nodal rotations 
std::vector< std::vector<double> >* SimoShellBase::Rn = NULL;

// Material director
const double SimoShellBase::MatDir[3] = {0.,0.,1.};

// Basis for material director increments
const double SimoShellBase::MatDirIncBasis[2][3] = {{1.,0.,0.},{0.,1.,0.}};


// Over-writes the default assembly procedure
// DOF array is not necessary
bool SimoShellBase::Assemble(std::vector<DResidue*>& OpArray, const LocalToGlobalMap& L2GMap, 
				    const Vec& Dofs, Vec* ResVec, Mat* DResMat, const bool APPEND)
{ return Assemble(OpArray, L2GMap, ResVec, DResMat, APPEND); }



// Assembles the residual and its variation
// The DOF array is not required since configuration updates are performed externally.
bool SimoShellBase::Assemble(std::vector<DResidue*>& OpArray, const LocalToGlobalMap& L2GMap,
			     Vec* ResVec, Mat* DResMat, const bool APPEND)
{
  PetscErrorCode ierr;
  if( APPEND==false )
    {
      ierr = VecSet(*ResVec, 0.); CHKERRQ(ierr);
      if( DResMat!=NULL )
	{ ierr = MatZeroEntries(*DResMat); CHKERRQ(ierr); }
    }

  std::vector< std::vector<double> > funcval;
  std::vector< std::vector< std::vector< std::vector<double> > > > dfuncval;
  const int nOperations = int(OpArray.size());
  const int nDof = int(OpArray[0]->GetFieldDof(0));
  int indices[nDof*nFields];
  double resvals[nDof*nFields];
  double dresvals[nDof*nFields*nDof*nFields];

  for(int e=0; e<nOperations; e++)
    {
      const SimoShellBase* Op = dynamic_cast<const SimoShellBase*>(OpArray[e]);
      if( Op==NULL )
	{
	  std::cerr<<"\nSimoShellBase::Assemble()- "
		   <<"Could not typecast operation.\n";
	  std::fflush( stdout ); return false;
	}
      
      // Compute residual and stiffness
      bool flag = false;
      if( DResMat==NULL )
	flag = Op->GetResidual(&funcval);
      else
	flag = Op->GetJacobian(&funcval, &dfuncval);
      if( flag == false )
	{
	  std::cerr<<"\nSimoShellBase::Assemble()- "
		   <<"Could not compute force/stiffness.\n";
	  std::fflush( stdout ); return false;
	}
      
      // Assemble residual
      for(int f=0; f<nFields; f++)
	for(int a=0; a<nDof; a++)
	  {
	    indices[nDof*f+a] = L2GMap.Map(f,a,e);
	    resvals[nDof*f+a] = funcval[f][a];
	  }
      ierr = VecSetValues(*ResVec, nFields*nDof, indices, resvals, ADD_VALUES);
      CHKERRQ(ierr);
      
      // Assemble stiffness
      if( DResMat!=NULL )
	{
	  for(int f=0; f<nFields; f++)
	    for(int a=0; a<nDof; a++)
	      for(int g=0; g<nFields; g++)
		for(int b=0; b<nDof; b++)
		  dresvals[nDof*nFields*nDof*f + nDof*nFields*a + nDof*g + b] = dfuncval[f][a][g][b];
	  ierr = MatSetValues(*DResMat, nFields*nDof, indices, nFields*nDof, indices, dresvals, ADD_VALUES);
	  CHKERRQ(ierr);
	}
    }
  return true;
  }



// Constructor with non-trivial initial strains
SimoShellBase::SimoShellBase(Element* IElm, const std::vector<int> inodes, 
			     const IsotropicLinearElasticShell3D& smat, 
			     const double refPhi[][3], const double REFt[][3])
  :DResidue(), Elm(IElm), SMat(&smat)
{
  // Check that configuration pointers have been set
  if( Rn==NULL || Xn==NULL )
    {
      std::cerr<<"\nSimoShellBase::SimoShellBase()- "
	       <<"Pointers for midsurface coordinates/directors have not been set.\n";
      std::fflush( stdout ); exit(1);
    }
  
  // Fields used
  FieldsUsed.resize(nFields);
  for(int f=0; f<nFields; f++)
    FieldsUsed[f] = f;
  
  // Number of quadrature points
  nQuad = int(Elm->GetIntegrationWeights(FieldsUsed[0]).size());
  
  // Number of dofs
  nDof = Elm->GetDof(FieldsUsed[0]);
  
  // Node numbers
  NodeNum.clear();
  for(int i=0; i<nDof; i++)
    NodeNum.push_back( inodes[i] );
  
  // Reference strains at quadrature points
  QuadRefStrains.clear();
  QuadRefStrains.resize(nQuad);
  
  // Normalize the nodal spatial directors
  double reft[nDof][SPD];
  for(int a=0; a<nDof; a++)
    {
      double norm = 0.;
      for(int k=0; k<SPD; k++)
	norm += REFt[a][k]*REFt[a][k];
      norm = sqrt(norm);
      if( norm<1.e-4 )
	{
	  std::cerr<<"\nSimoShellBase::SimoShellBase()- "
		   <<"Norm of nodal director was close to 0.\n";
	  std::fflush( stdout ); exit(1);
	}
      for(int k=0; k<SPD; k++)
	reft[a][k] = REFt[a][k]/norm;
    }


  // Initialize mid-surface coordinates and rotations for this operation
  for(int a=0; a<nDof; a++)
    {
      for(int k=0; k<SPD; k++)
	(*Xn)[NodeNum[a]][k] = refPhi[a][k];
      
      if( !ComputeRotationMatrix(MatDir, reft[a], &((*Rn)[NodeNum[a]][0]) ) )
	{
	  std::cerr<<"\nSimoShellBase::DynamicSimoShellBase()- "
		   <<"Could not compute initial rotation matrix.\n";
	  std::fflush( stdout ); exit(1);
	}
    }

  // Initialize reference strains at quadrature points
  const double Identity[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  for(int q=0; q<nQuad; q++)
    {
      // Point data at this quadrature point
      PointData PD;
      
      // Tangents
      for(int i=0; i<SPD; i++)
	for(int p=0; p<PDIM; p++)
	  {
	    PD.dphi[i][p] = 0.;
	    for(int a=0; a<nDof; a++)
	      PD.dphi[i][p] += refPhi[a][i]*Elm->GetDShape(FieldsUsed[0], q, a, p);
	  }
      
      // Interpolate directors and derivatives
      double tinterp[SPD] = {0.,0.,0.};
      double dtinterp[SPD][PDIM] = { {0.,0.}, {0.,0.}, {0.,0.} };
      for(int a=0; a<nDof; a++)
	for(int k=0; k<SPD; k++)
	  {
	    tinterp[k] += reft[a][k]*Elm->GetShape(FieldsUsed[0], q, a);
	    for(int p=0; p<PDIM; p++)
	      dtinterp[k][p] += reft[a][k]*Elm->GetDShape(FieldsUsed[0], q, a, p);
	  }
      
      // Norm of interpolated director
      double norm = 0.;
      for(int k=0; k<SPD; k++)
	norm += tinterp[k]*tinterp[k];
      norm = sqrt(norm);
      if( norm<1.e-4 )
	{
	  std::cerr<<"\nSimoShellBase::SimoShellBase()- "
		   <<"Interpolated director at gauss point has close to zero norm.\n";
	  std::fflush( stdout ); exit(1);
	}
      
      // Director at gauss point
      for(int k=0; k<SPD; k++)
	PD.t[k] = tinterp[k]/norm;
      
      // Projection matrix
      double P[SPD][SPD];
      for(int i=0; i<SPD; i++)
	for(int j=0; j<SPD; j++)
	  P[i][j] = Identity[i][j] - PD.t[i]*PD.t[j];
      
      // Director derivatives
      for(int i=0; i<SPD; i++)
	for(int p=0; p<PDIM; p++)
	  {
	    PD.tprime[i][p] = 0.;
	    for(int j=0; j<SPD; j++)
	      PD.tprime[i][p] += P[i][j]*dtinterp[j][p]/norm;
	  }
      
      if( !QuadRefStrains[q].Initialize(PD) )
	{
	  std::cerr<<"\nSimoShellBase::SimoShellBase()- "
		   <<"Could not compute reference strains at quadrature points.\n";
	  std::fflush( stdout ); exit(1);
	}
    }
}



//! Destructor
SimoShellBase::~SimoShellBase() {}
  
// Copy constructor
SimoShellBase::SimoShellBase(const SimoShellBase& Obj)
  :DResidue(), Elm(Obj.Elm), SMat(Obj.SMat)
{
  FieldsUsed.clear(); FieldsUsed = Obj.FieldsUsed; 
  nQuad = Obj.nQuad;
  nDof = Obj.nDof;
  NodeNum.clear();
  for(int i=0; i<nDof; i++)
    NodeNum.push_back( Obj.NodeNum[i] );
  
  QuadRefStrains.clear();
  QuadRefStrains.resize(nQuad);
  for(int q=0; q<nQuad; q++)
    QuadRefStrains[q] = Obj.QuadRefStrains[q];
}
 


// Access to the element
const Element* SimoShellBase::GetElement() const
{ return Elm; }
  

// Returns the node numbers for this operation
std::vector<int> SimoShellBase::GetOperationNodeNumbers() const
{ return NodeNum; }
    

// Returns the spatial director at a node 
std::vector<double> SimoShellBase::GetSpatialDirectorAtNode(const int a) const
{
  std::vector<double> t(SPD);
  for(int i=0; i<SPD; i++)
    {
      t[i] = 0.;
      for(int j=0; j<SPD; j++)
	t[i] += (*Rn)[NodeNum[a]][SPD*i+j]*MatDir[j];
    }
  return t;
}
  

// Returns the rotation matrix at a node 
std::vector<double> SimoShellBase::GetRotationMatrixAtNode(const int a) const
{ return (*Rn)[NodeNum[a]]; }
  

// Returns the midsurface position at a node
std::vector<double> SimoShellBase::GetMidSurfaceCoordinatesAtNode(const int a) const
{ return (*Xn)[NodeNum[a]]; }


// Access to the shell material
const IsotropicLinearElasticShell3D* SimoShellBase::GetShellMaterial() const
{ return SMat; }
  
// Set the shell material
void SimoShellBase::SetShellMaterial(const IsotropicLinearElasticShell3D& smat)
{ SMat = &smat; }

// Access to the fields used
const std::vector<int>& SimoShellBase::GetField() const
{ return FieldsUsed; }

// Returns the number of dofs in given field
int SimoShellBase::GetFieldDof(const int fieldnum) const
{ return Elm->GetDof(FieldsUsed[fieldnum]); }
  

// Default routines for computing the residual: calls GetResidual
void SimoShellBase::GetVal(const void* argval,
			   std::vector< std::vector<double> >* funcval) const
{ GetResidual(funcval); }
  

// Default routine for computing the residual and stiffness: Calls GetJacobian
void SimoShellBase::
GetDVal(const void* argval,
	std::vector< std::vector<double> >* funcval, 
	std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval) const
{  GetJacobian(funcval, dfuncval); }


// Returns the material director field, which is assumed to be a constant
bool SimoShellBase::GetMaterialDirector(double* E) const
{
  for(int i=0; i<SPD; i++)
    E[i] = MatDir[i];
  return true;
}


// Returns the basis for material director increments
bool SimoShellBase::GetMaterialDirectorIncrementBasis(double Eperp[][3]) const
{
  for(int i=0; i<PDIM; i++)
    for(int j=0; j<SPD; j++)
      Eperp[i][j] = MatDirIncBasis[i][j];
  return true;
}


// Resize and initialize force and stiffness
void SimoShellBase::ResizeAndInitializeForceAndStiffness
(std::vector< std::vector<double> >* funcval, 
 std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval) const
{
  if( funcval!=NULL )
    {
      if( int(funcval->size())<nFields )
	funcval->resize(nFields);
      for(int f=0; f<nFields; f++)
	{
	  if( int((*funcval)[f].size())<nDof )
	    (*funcval)[f].resize(nDof);
	  for(int a=0; a<nDof; a++)
	    (*funcval)[f][a] = 0.;
	}
    }
  
  if( dfuncval!=NULL )
    {
      if( int(dfuncval->size())<nFields )
	dfuncval->resize(nFields);
      for(int f=0; f<nFields; f++)
	{
	  if( int((*dfuncval)[f].size())<nDof )
	    (*dfuncval)[f].resize(nDof);
	  for(int a=0; a<nDof; a++)
	    {
	      if( int((*dfuncval)[f][a].size())<nFields )
		(*dfuncval)[f][a].resize(nFields);
	      for(int g=0; g<nFields; g++)
		{
		  if( int((*dfuncval)[f][a][g].size())<nDof )
		    (*dfuncval)[f][a][g].resize(nDof);
		  for(int b=0; b<nDof; b++)
		    (*dfuncval)[f][a][g][b] = 0.;
		}
	    }
	}
    }
}



// Maps material director dofs to spatial director dofs
void SimoShellBase::MapMaterialDirectorDofsToSpatialDirectorDofs
(const double* R, const double* Tdof, double* tdof)
{
  double Tvec[SPD];
  for(int i=0; i<SPD; i++)
    {
      Tvec[i] = 0.;
      for(int p=0; p<PDIM; p++)
	Tvec[i] += Tdof[p]*MatDirIncBasis[p][i];
    }
  
  for(int i=0; i<SPD; i++)
    {
      tdof[i] = 0.;
      for(int j=0; j<SPD; j++)
	tdof[i] += R[SPD*i+j]*Tvec[j];
    }
}


// Computes the exponential map 
void SimoShellBase::ExpSO3(const double* theta, double* mat)
{
  double Identity[SPD][SPD] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  double normtheta = 0.;
  for(int i=0; i<SPD; i++)
    normtheta += theta[i]*theta[i];
  normtheta = sqrt(normtheta);
  
  double j0 = gsl_sf_bessel_j0(normtheta);
  double sqj0by2 = pow(gsl_sf_bessel_j0(normtheta/2.),2.);

  // Skew-symmetric matrix with axial vector theta
  double thetahat[SPD][SPD] = {{0.,        -theta[2], theta[1]},
			       {theta[2],  0.,        -theta[0]},
			       {-theta[1], theta[0],  0.}};
  
  // Square of thetahat
  double thetahat2[SPD][SPD];
  for(int i=0; i<SPD; i++)
    for(int j=0; j<SPD; j++)
      {
	thetahat2[i][j] = 0.;
	for(int k=0; k<SPD; k++)
	  thetahat2[i][j] += thetahat[i][k]*thetahat[k][j];
      }

  for(int i=0; i<SPD; i++)
    for(int j=0; j<SPD; j++)
      mat[SPD*i+j] = Identity[i][j] + j0*thetahat[i][j] + 0.5*sqj0by2*thetahat2[i][j];
}



// Computes the rotation matrix
bool SimoShellBase::ComputeRotationMatrix(const double* E, const double* reft, double* R)
{
  const double Identity[SPD][SPD] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  
  // Normalize spatial director
  double norm = 0.;
  for(int k=0; k<SPD; k++)
    norm += reft[k]*reft[k];
  norm = sqrt(norm);
  if( norm<1.e-4 )
    {
      std::cerr<<"\nSimoShellBase::ComputeRotationMatrix()- "
	       <<"Provided nodal director has close to zero norm.\n";
      std::fflush( stdout ); return false;
    }
  double t[SPD];
  for(int k=0; k<SPD; k++)
    t[k] = reft[k]/norm;
      
  // Dot(E, t)
  double Edott = 0.;
  for(int k=0; k<SPD; k++)
    Edott += t[k]*E[k];
  if( fabs(Edott+1.)<1.e-4 )
    {
      std::cerr<<"\nSimoShellBase::ComputeRotationMatrix()- "
	       <<"Provided vectors are anti-parallel.\n";
      std::fflush( stdout ); return false;
    }
      
  // Cross(E, t)
  double theta[SPD];
  for(int i=0; i<SPD; i++)
    theta[i] = E[(i+1)%SPD]*t[(i+2)%SPD] - E[(i+2)%SPD]*t[(i+1)%SPD];
  
  // Skew-symmetric matrix
  double thetahat[SPD][SPD] = {{0.,        -theta[2], theta[1]},
			       {theta[2],  0.,        -theta[0]},
			       {-theta[1], theta[0],  0.}};
  
  // Rotation matrix
  for(int i=0; i<SPD; i++)
    for(int j=0; j<SPD; j++)
      R[SPD*i+j] = 
	Edott*Identity[i][j] + thetahat[i][j] + theta[i]*theta[j]/(1.+Edott);
  
  return true;
}
