// Sriramajayam

#include "IsotropicLinearElasticShell3D.h"

// Constructor
IsotropicLinearElasticShell3D::
IsotropicLinearElasticShell3D(const double E, const double nu, const double h, const double iRho)
:YoungModulus(E), PoissonRatio(nu), thickness(h)
{
  Rho = iRho*thickness;
  MomentOfInertia = iRho*pow(thickness,3.)/12.;
  Kmem = 0.5*YoungModulus*h/(1.-pow(PoissonRatio,2.));
  Kbend = YoungModulus*h*h*h/(12.*(1-pow(PoissonRatio,2.)));
  Kshear = 0.5*YoungModulus*h/(1.+PoissonRatio);
}

// Copy constructor
IsotropicLinearElasticShell3D::
IsotropicLinearElasticShell3D(const IsotropicLinearElasticShell3D& Obj)
:YoungModulus(Obj.YoungModulus), PoissonRatio(Obj.PoissonRatio), thickness(Obj.thickness),
  Rho(Obj.Rho), MomentOfInertia(Obj.MomentOfInertia),
  Kmem(Obj.Kmem), Kbend(Obj.Kbend), Kshear(Obj.Kshear) {}

// Destructor
IsotropicLinearElasticShell3D::~IsotropicLinearElasticShell3D() {}

// Cloning
IsotropicLinearElasticShell3D* IsotropicLinearElasticShell3D::Clone() const
{ return new IsotropicLinearElasticShell3D(*this); }


// Returns the Young's modulus
double IsotropicLinearElasticShell3D::GetYoungModulus() const
{ return YoungModulus; }

// Returns the Poisson ratio
double IsotropicLinearElasticShell3D::GetPoissonRatio() const
{ return PoissonRatio; }


// Returns the Poisson ratio
double IsotropicLinearElasticShell3D::GetThickness() const
{ return thickness; }

// Returns the density of the surface in the reference configuration
double IsotropicLinearElasticShell3D::GetSurfaceDensity() const
{ return Rho; }
  
// Returns the moment of inertia
double IsotropicLinearElasticShell3D::GetMomentOfInertia() const
{ return MomentOfInertia; }

// Set the moment of inertia
void IsotropicLinearElasticShell3D::SetMomentOfInertia(const double I0)
{ MomentOfInertia = I0; }
  
// Returns the surface density in the reference configuration
void IsotropicLinearElasticShell3D::SetSurfaceDensity(const double density)
{ Rho = density; }


// Set the membrane modulus
void IsotropicLinearElasticShell3D::SetMembraneModulus(const double Modulus)
{ Kmem = Modulus; }

// Set the bending modulus
void IsotropicLinearElasticShell3D::SetBendingModulus(const double Modulus)
{ Kbend = Modulus; }
  
// Set the shear modulus
void IsotropicLinearElasticShell3D::SetShearModulus(const double Modulus)
{ Kshear = Modulus; }

// Return the membrane modulus
double IsotropicLinearElasticShell3D::GetMembraneModulus() const
{ return Kmem; }

// Return the bending modulus
double IsotropicLinearElasticShell3D::GetBendingModulus() const
{ return Kbend; }

// Returns the shear modulus
double IsotropicLinearElasticShell3D::GetShearModulus() const
{ return Kshear; }

// H matrix
bool IsotropicLinearElasticShell3D::GetHMatrix(const double ginv[][2],
					       double HMatrix[][2][2][2]) const
{
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++)
      for(int k=0; k<2; k++)
	for(int L=0; L<2; L++)
	  HMatrix[i][j][k][L] = 
	    PoissonRatio*ginv[i][j]*ginv[k][L] + 
	    0.5*(1.-PoissonRatio)*( ginv[i][k]*ginv[j][L] + ginv[i][L]*ginv[j][k] );

  return true;
}


// Returns the membrane stiffness modulii
bool IsotropicLinearElasticShell3D::GetMembraneStiffness(const double ginv[][2], 
							 double modulii[][2][2][2]) const
{
  double H[2][2][2][2];
  if( !GetHMatrix(ginv, H) )
    {
      std::cerr<<"\nIsotropicLinearElasticShell3D::GetMembraneConstitutiveResponse()- "
	       <<"Could not compute H matrix.\n";
      std::fflush( stdout ); exit(1);
    }
  
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++)
      for(int k=0; k<2; k++)
	for(int L=0; L<2; L++)
	  modulii[i][j][k][L] = Kmem*H[i][j][k][L];

  return true;
}

// Returns the bending stiffness modulii
bool IsotropicLinearElasticShell3D::GetBendingStiffness(const double ginv[][2], 
							double modulii[][2][2][2]) const
{
   double H[2][2][2][2];
   if( !GetHMatrix(ginv, H) )
    {
      std::cerr<<"\nIsotropicLinearElasticShell3D::GetMembraneConstitutiveResponse()- "
	       <<"Could not compute H matrix.\n";
      std::fflush( stdout ); exit(1);
    }
  
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++)
      for(int k=0; k<2; k++)
	for(int L=0; L<2; L++)
	  modulii[i][j][k][L] = Kbend*H[i][j][k][L];
  
  return true;
}
  

// Returns the shear stiffness modulii
bool IsotropicLinearElasticShell3D::GetShearStiffness(const double ginv[][2],
						      double modulii[][2]) const
{
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++)
      modulii[i][j] = Kshear*ginv[i][j];
  return true;
}


// Computes the membrane stress resultants and modulii
bool IsotropicLinearElasticShell3D::
GetMembraneConstitutiveResponse(const double ginv[][2],
				const double strain[][2],
				double stress[][2], 
				double modulii[][2][2][2]) const
{
  double HMatrix[2][2][2][2];
  if( !GetHMatrix(ginv, HMatrix) )
    {
      std::cerr<<"\nIsotropicLinearElasticShell3D::GetMembraneConstitutiveResponse()- "
	       <<"Could not compute H matrix.\n";
      std::fflush( stdout ); return false;
    }

  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++)
      {
	stress[i][j] = 0.;
	for(int k=0; k<2; k++)
	  for(int L=0; L<2; L++)
	    {
	      stress[i][j] += Kmem*HMatrix[i][j][k][L]*strain[k][L];
	      if( modulii!=NULL )
		modulii[i][j][k][L] = Kmem*HMatrix[i][j][k][L];
	    }
      }
  
  return true;
}



// Computes the bending stress resultants and modulii
bool IsotropicLinearElasticShell3D::
GetBendingConstitutiveResponse(const double ginv[][2],
			       const double strain[][2],
			       double stress[][2],
			       double modulii[][2][2][2]) const
{
  // Compute the H matrix
  double HMatrix[2][2][2][2];
  if( !GetHMatrix(ginv, HMatrix) )
    {
      std::cerr<<"\nIsotropicLinearElasticShell3D::GetBendingConstitutiveResponse()- "
	       <<"Could not compute H matrix.\n";
      std::fflush( stdout ); return false;
    }
  
  // Compute stress and return modulii if requested
  for(int i=0; i<2; i++)
    for(int j=0; j<2; j++)
      {
	stress[i][j] = 0.;
	for(int k=0; k<2; k++)
	  for(int L=0; L<2; L++)
	    {
	      stress[i][j] += Kbend*HMatrix[i][j][k][L]*strain[k][L];
	      if( modulii!=NULL )
		modulii[i][j][k][L] = Kbend*HMatrix[i][j][k][L];
	    }
      }
  
  return true;
}


// Computes the shear stress resultants and modulii
bool IsotropicLinearElasticShell3D::
GetShearConstitutiveResponse(const double ginv[][2],
			     const double *strain,
			     double* stress,
			     double modulii[][2]) const
{
  for(int i=0; i<2; i++)
    {
      stress[i] = 0.;
      for(int j=0; j<2; j++)
	{
	  stress[i] += Kshear*ginv[i][j]*strain[j];
	  if( modulii!=NULL )
	    modulii[i][j] = Kshear*ginv[i][j];
	}
    }
  return true;
}

// Computes the strain energy for given strain measures
bool IsotropicLinearElasticShell3D::ComputeStrainEnergyDensity(const double ginv[][2],
							       const double memstrain[][2], 
							       const double* shearstrain, 
							       const double bendstrain[][2], 
							       double& W) const
{
  W = 0.;
  double shearstress[2];
  double memstress[2][2];
  double bendstress[2][2];
  if( !GetShearConstitutiveResponse(ginv, shearstrain, shearstress) ||
      !GetMembraneConstitutiveResponse(ginv, memstrain, memstress) ||
      !GetBendingConstitutiveResponse(ginv, bendstrain, bendstress) )
    {
      std::cerr<<"\nIsotropicLinearElasticShell3D::ComputeStrainEnergyDensity()- "
	       <<"Could not compute constitutive response.\n";
      std::fflush( stdout ); return false;
    }
  
  for(int i=0; i<2; i++)
    {
      W += 0.5*shearstress[i]*shearstrain[i];
      for(int j=0; j<2; j++)
	W += 0.5*(memstress[i][j]*memstrain[i][j] + bendstress[i][j]*bendstrain[i][j]);
    }
  return true;
}
