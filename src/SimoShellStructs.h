// Sriramajayam

#ifndef SIMOSHELLSTRUCTS
#define SIMOSHELLSTRUCTS

#include <iostream>
#include <vector>
#include <cstdio>

namespace SimoShell
{
  //! Point data (midsurface coordinates and  director information)
  struct PointData
  {
    PointData();
    PointData(const PointData& Obj);
    PointData& operator=(const PointData& dd);
    double dphi[3][2];
    double t[3];
    double tprime[3][2];
  };

  //! First variation of point data
  typedef PointData VarPointData;
  
  //! Second variation of point data
  typedef PointData VarVarPointData;
  
  //! Reference strains ( at quadarture points )
  struct ReferenceStrains
  {
    ReferenceStrains();
    ReferenceStrains(const ReferenceStrains& Obj);
    ReferenceStrains& operator=(const ReferenceStrains& rhs);

    double ref_memstrain[2][2];
    double ref_bendstrain[2][2];
    double ref_shearstrain[2];
    double ginv[2][2];
    
    bool Initialize(const PointData& PD);
  };
  


  // Data for strain calculations
  struct StrainData
  {
    StrainData();
    StrainData(const StrainData& Obj);
    StrainData& operator=(const StrainData& rhs);
    
    double memstrain[2][2];
    double bendstrain[2][2];
    double shearstrain[2];
    
    bool Initialize(const ReferenceStrains& rs, const PointData& ptdata);
    
    double ComputeVirtualWork(const double* shearstress, 
			      const double memstress[][2], 
			      const double bendstress[][2]) const;
  };
  
 
  // First variation of strains
  struct VarStrainData
  {
    VarStrainData();
    VarStrainData(const VarStrainData& Obj);
    VarStrainData& operator=(const VarStrainData& rhs);
    
    double var_shearstrain[2];
    double var_memstrain[2][2];
    double var_bendstrain[2][2];
    
    bool Initialize(const PointData& ptdata, const VarPointData& varptdata);
    
    double ComputeVarVirtualWork(const double* shearstress, 
				 const double memstress[][2], 
				 const double bendstress[][2]) const;
  };
  
  // Second variation of strains
  struct VarVarStrainData
  {
    VarVarStrainData();
    VarVarStrainData(const VarVarStrainData& Obj);
    VarVarStrainData& operator=(const VarVarStrainData& rhs);
    
    double VAR_var_shearstrain[2];
    double VAR_var_memstrain[2][2];
    double VAR_var_bendstrain[2][2];
    
    bool Initialize(const PointData& ptdata,
		    const VarPointData& var, const VarPointData& VAR, 
		    const VarVarPointData& VAR_var);

    double ComputeVarVarVirtualWork(const double* shearstress, 
				    const double memstress[][2], 
				    const double bendstress[][2], 
				    const double Ashear[][2], 
				    const double Amem[][2][2][2],
				    const double Abend[][2][2][2], 
				    const VarStrainData& var,
				    const VarStrainData& VAR) const;
  };
  
}


#endif
