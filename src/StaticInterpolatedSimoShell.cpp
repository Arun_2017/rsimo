// Sriramajayam

#include "StaticInterpolatedSimoShell.h"

// Update the midsurface and rotations
bool StaticInterpolatedSimoShell::UpdateConfiguration(const std::vector< std::vector<double> >& dX, 
						      const std::vector< std::vector<double> >& dT)
{
  const int nNodes = int(Xn->size());
  double Identity[SPD][SPD] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,1.}};
  for(int n=0; n<nNodes; n++)
    {
      // Update midsurface
      for(int k=0; k<SPD; k++)
	(*Xn)[n][k] += dX[n][k];
      
      // Existing rotation at this node
      std::vector<double> Rmat = (*Rn)[n];
      
      // Existing spatial director
      double t[SPD];
      for(int i=0; i<SPD; i++)
	{
	  t[i] = 0.;
	  for(int j=0; j<SPD; j++)
	    t[i] += Rmat[SPD*i+j]*MatDir[j];
	}
      
      // Increment in nodal spatial directors
      double dt[SPD];
      MapMaterialDirectorDofsToSpatialDirectorDofs(&Rmat[0], &dT[n][0], dt);
      
      // t x dt
      double theta[SPD];
      double normtheta = 0.;
      for(int i=0; i<SPD; i++)
	{
	  theta[i] = t[(i+1)%SPD]*dt[(i+2)%SPD] - t[(i+2)%SPD]*dt[(i+1)%SPD];
	  normtheta += theta[i]*theta[i];
	}
      normtheta = sqrt(normtheta);
      if( fabs(sqrt(dt[0]*dt[0]+dt[1]*dt[1]+dt[2]*dt[2])-normtheta)>1.e-6 )
	{
	  std::cerr<<"\nStaticInterpolatedSimoShell::UpdateConfiguration()- "
		   <<"Norm of dt and theta vectors were not equal.\n";
	  std::fflush( stdout );
	}
      double j0 = gsl_sf_bessel_j0(normtheta);
      double sqj0by2 = pow(gsl_sf_bessel_j0(normtheta/2.),2.);
      
      // Skew-symmetric matrix and its square
      double thetahat[SPD][SPD] = {{0.,        -theta[2], theta[1]},
				   {theta[2],  0.,        -theta[0]},
				   {-theta[1], theta[0],  0.}};
      double thetahat2[SPD][SPD];
      for(int i=0; i<SPD; i++)
	for(int j=0; j<SPD; j++)
	  {
	    thetahat2[i][j] = 0.;
	    for(int k=0; k<SPD; k++)
	      thetahat2[i][j] += thetahat[i][k]*thetahat[k][j];
	  }
      
      // Increment in rotation: exp(thetahat)
      double dR[SPD*SPD];
      for(int i=0; i<SPD; i++)
	for(int j=0; j<SPD; j++)
	  dR[SPD*i+j] = Identity[i][j] + j0*thetahat[i][j] + 0.5*sqj0by2*thetahat2[i][j];
      
      // New rotation
      double Rnew[SPD*SPD];
      for(int i=0; i<SPD; i++)
	for(int j=0; j<SPD; j++)
	  {
	    Rnew[SPD*i+j] = 0.;
	    for(int k=0; k<SPD; k++)
	      Rnew[SPD*i+j] += dR[SPD*i+k] * Rmat[SPD*k+j];
	  }
      
      // Update
      for(int i=0; i<SPD; i++)
	for(int j=0; j<SPD; j++)
	  (*Rn)[n][SPD*i+j] = Rnew[SPD*i+j];
    }
  return true;
}

  


// Constructor with non-trivial initial strains
StaticInterpolatedSimoShell::
StaticInterpolatedSimoShell(Element* IElm, const std::vector<int>& inodes,
			    const IsotropicLinearElasticShell3D& smat, 
			    const double refPhi[][3], const double reft[][3])
  :SimoShellBase(IElm, inodes, smat, refPhi, reft) {}


// Destructor
StaticInterpolatedSimoShell::~StaticInterpolatedSimoShell() {}


// Copy constructor
StaticInterpolatedSimoShell::StaticInterpolatedSimoShell(const StaticInterpolatedSimoShell& Obj)
  :SimoShellBase(Obj) {}
  

// Cloning
StaticInterpolatedSimoShell* StaticInterpolatedSimoShell::Clone() const
{ return new StaticInterpolatedSimoShell(*this); }


// Computes the functional being minimized
bool StaticInterpolatedSimoShell::ComputeFunctional(double& Fval) const
{
  Fval = 0.;
  double shearstress[PDIM];
  double memstress[PDIM][PDIM];
  double bendstress[PDIM][PDIM];
  const std::vector<double> Qwts = Elm->GetIntegrationWeights(FieldsUsed[0]);
  SimoShell::StrainData SD;
  SimoShell::PointData PD;
  
  // Directors at this element
  double tnodal[nDof][SPD];
  for(int a=0; a<nDof; a++)
    for(int i=0; i<SPD; i++)
      {
	tnodal[a][i] = 0.;
	for(int j=0; j<SPD; j++)
	  tnodal[a][i] += (*Rn)[NodeNum[a]][SPD*i+j] * MatDir[j];
      }
  
  for(int q=0; q<nQuad; q++)
    {
      // At this point:
      for(int f=0; f<SPD; f++)
	{
	  PD.t[f] = 0.;
	  for(int a=0; a<nDof; a++)
	    PD.t[f] += tnodal[a][f]*Elm->GetShape(0, q, a);
	  
	for(int p=0; p<PDIM; p++)
	  {
	    PD.tprime[f][p] = 0.;
	    PD.dphi[f][p] = 0.;
	    for(int a=0; a<nDof; a++)
	      {
		PD.dphi[f][p] += (*Xn)[NodeNum[a]][f]*Elm->GetDShape(0, q, a, p);
		PD.tprime[f][p] += tnodal[a][f]*Elm->GetDShape(0, q, a, p);
	      }
	  }
	}
      
      // Compute strains
      if(!SD.Initialize(QuadRefStrains[q], PD))
	{
	  std::cerr<<"\nStaticInterpolatedSimoShell::ComputeFunctional()- "
		   <<"Could not compute strains.\n";
	  std::fflush( stdout ); return false;
	}
      
      // Compute stresses
      if( !SMat->GetMembraneConstitutiveResponse(QuadRefStrains[q].ginv, SD.memstrain, memstress) ||
	  !SMat->GetBendingConstitutiveResponse(QuadRefStrains[q].ginv, SD.bendstrain, bendstress) ||
	  !SMat->GetShearConstitutiveResponse(QuadRefStrains[q].ginv, SD.shearstrain, shearstress) )
	{
	  std::cerr<<"\nStaticInterpolatedSimoShell::ComputeFunctional()- "
		   <<"Could not compute material response.\n";
	  std::fflush( stdout ); return false;
	}
      
      // Virtual work
      double W = SD.ComputeVirtualWork(shearstress, memstress, bendstress);
      
      // Area measure (in the reference configuration)
      double detginv = 
	QuadRefStrains[q].ginv[0][0]*QuadRefStrains[q].ginv[1][1] - 
	QuadRefStrains[q].ginv[0][1]*QuadRefStrains[q].ginv[1][0];
      double dA0 = Qwts[q]/sqrt(detginv); 
      
      // Update functional
      Fval += W*dA0;
    }
  return true;
}


// Computes the force vector
bool StaticInterpolatedSimoShell::GetResidual(std::vector< std::vector<double> >* funcval) const
{ return GetJacobian(funcval, NULL); }
  

// Computes the force vector and stiffness matrix
bool StaticInterpolatedSimoShell::
GetJacobian(std::vector< std::vector<double> >* funcval, 
	    std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval) const
{
  ResizeAndInitializeForceAndStiffness(funcval, dfuncval);
  
  double memstress[PDIM][PDIM];
  double bendstress[PDIM][PDIM];
  double shearstress[PDIM];
  double Amem[PDIM][PDIM][PDIM][PDIM];
  double Abend[PDIM][PDIM][PDIM][PDIM];
  double Ashear[PDIM][PDIM];
  
  const std::vector<double> Qwts = Elm->GetIntegrationWeights(FieldsUsed[0]);

  // Shape functions and derivatives
  std::vector<double> Shp = Elm->GetShape(0);
  std::vector<double> DShp = Elm->GetDShape(0);
  
  // Spatial directors at the nodes
  double tnodal[nDof][SPD];
  for(int a=0; a<nDof; a++)
    for(int i=0; i<SPD; i++)
      {
	tnodal[a][i] = 0.;
	for(int j=0; j<SPD; j++)
	  tnodal[a][i] += (*Rn)[NodeNum[a]][SPD*i+j]*MatDir[j];
      }

  SimoShell::PointData PD;
  SimoShell::StrainData SD;
  std::vector< std::vector<SimoShell::VarPointData> > varPD(nFields);
  std::vector< std::vector< SimoShell::VarStrainData> > varSD(nFields);
  for(int f=0; f<nFields; f++)
    {
      varPD[f].resize(nDof);
      varSD[f].resize(nDof);
    }
  SimoShell::VarVarPointData VARvarPD;
  SimoShell::VarVarStrainData VARvarSD;
  
  // Precompute second variations of nodal dofs = -(-var_tA , VAR_tA) tA
  // since it depends only on nodal data and is independent of quadrature.
  // VAR_{gb}(var_{fa}(t_dof)) is nontrivial only for a = b = dof
  double VAR_var_nodal_t[nDof][SPD][PDIM][PDIM];
  for(int dof=0; dof<nDof; dof++)
    for(int f=0; f<PDIM; f++)
      {
	double vT[PDIM] = {0.,0.};
	vT[f] = 1.;
	double vt[SPD];
	MapMaterialDirectorDofsToSpatialDirectorDofs(&((*Rn)[NodeNum[dof]][0]), vT, vt);
	
	for(int g=0; g<PDIM; g++)
	  {
	    double VT[PDIM] = {0.,0.};
	    VT[g] = 1.;
	    double Vt[PDIM];
	    MapMaterialDirectorDofsToSpatialDirectorDofs(&((*Rn)[NodeNum[dof]][0]), VT, Vt);
	    
	    double dot = 0.;
	    for(int k=0; k<SPD; k++)
	      dot += vt[k]*Vt[k];
	    
	    for(int k=0; k<SPD; k++)
	      VAR_var_nodal_t[dof][k][f][g] = -dot*tnodal[dof][k];
	  }
      }
  
  for(int q=0; q<nQuad; q++)
    {
      // Surface tangents and directors at this quadrature point
      for(int i=0; i<SPD; i++)
	{
	  PD.t[i] = 0.;
	  for(int a=0; a<nDof; a++)
	    PD.t[i] += tnodal[a][i]*Shp[nDof*q+a]; 
	  
	  for(int p=0; p<PDIM; p++)
	    {
	      PD.dphi[i][p] = 0.;
	      PD.tprime[i][p] = 0.;
	      for(int a=0; a<nDof; a++)
		{
		  PD.dphi[i][p]   += (*Xn)[NodeNum[a]][i]*DShp[nDof*PDIM*q+PDIM*a+p];
		  PD.tprime[i][p] += tnodal[a][i]*DShp[nDof*PDIM*q+PDIM*a+p];
		}
	    }
	}
      
      // Compute strains
      if( !SD.Initialize(QuadRefStrains[q], PD) )
	{
	  std::cerr<<"\nStaticInterpolatedSimoShell::GetDVal()- "
		   <<"Could not compute strains at quadrature points.\n";
	  std::fflush( stdout ); return false;
	}
      
      // Compute stresses
      bool flag = false;
      if( dfuncval==NULL )
	flag = 
	  SMat->GetMembraneConstitutiveResponse(QuadRefStrains[q].ginv, SD.memstrain, memstress) &&
	  SMat->GetBendingConstitutiveResponse(QuadRefStrains[q].ginv, SD.bendstrain, bendstress) &&
	  SMat->GetShearConstitutiveResponse(QuadRefStrains[q].ginv, SD.shearstrain, shearstress);
      else
	flag = 
	  SMat->GetMembraneConstitutiveResponse(QuadRefStrains[q].ginv, SD.memstrain, memstress, Amem) &&
	  SMat->GetBendingConstitutiveResponse(QuadRefStrains[q].ginv, SD.bendstrain, bendstress, Abend) &&
	  SMat->GetShearConstitutiveResponse(QuadRefStrains[q].ginv, SD.shearstrain, shearstress, Ashear);
      if( !flag )
	{
	  std::cerr<<"\nStaticInterpolatedSimoShell::GetDVal()- "
		   <<"Could not compute constitutive response.\n";
	  std::fflush( stdout ); return false;
	}

      // Area measure (in the reference configuration)
      const double detginv = 
	QuadRefStrains[q].ginv[0][0]*QuadRefStrains[q].ginv[1][1] -
	QuadRefStrains[q].ginv[0][1]*QuadRefStrains[q].ginv[1][0];
      const double dA0 = Qwts[q]/sqrt(detginv); 
      
      // Variation of membrane dofs
      for(int f=0; f<nFields; f++)
	for(int a=0; a<nDof; a++)
	  {
	    // Variation in surface tangents and zero variation in directors at this point
	    for(int i=0; i<SPD; i++)
	      {
		varPD[f][a].t[i] = 0;
		for(int j=0; j<PDIM; j++)
		  {
		    varPD[f][a].dphi[i][j] = 0.;
		    varPD[f][a].tprime[i][j] = 0.;
		  }
	      }

	    // Membrane variations
	    if( f<SPD )
	      for(int j=0; j<PDIM; j++)
		varPD[f][a].dphi[f][j] = DShp[nDof*PDIM*q+PDIM*a+j];
	    else
	      {
		// Director variations

		// Material Dof of this variation
		double vTdof[PDIM] = {0.,0.};
		vTdof[f-SPD] = 1.;
		
		// Spatial dof of this variation
		double vtdof[SPD];
		MapMaterialDirectorDofsToSpatialDirectorDofs(&((*Rn)[NodeNum[a]][0]), vTdof, vtdof);
		
		// Spatial director variation and its derivatives at this gauss point
		for(int i=0; i<SPD; i++)
		  {
		    varPD[f][a].t[i] = vtdof[i]*Shp[nDof*q+a];
		    for(int p=0; p<PDIM; p++)
		      varPD[f][a].tprime[i][p] = vtdof[i]*DShp[nDof*PDIM*q+PDIM*a+p];
		  }
	      }

	    
	    // Variation in strains
	    if( !varSD[f][a].Initialize(PD, varPD[f][a]) )
	      {
		std::cerr<<"\nStaticInterpolatedSimoShell3D::GetDVal()- "
			 <<"Could not compute variations of strains.\n";
		std::fflush( stdout ); return false;
	      }
	    
	    // Update residue
	    (*funcval)[f][a] += dA0*varSD[f][a].ComputeVarVirtualWork(shearstress, memstress, bendstress);
	  }
      
      
      // Compute stiffness
      if( dfuncval!=NULL )
	for(int f=0; f<nFields; f++)
	  for(int a=0; a<nDof; a++)
	    for(int g=0; g<nFields; g++)
	      for(int b=0; b<nDof; b++)
		{
		  // Second variation of spatial director and its derivatives
		  for(int i=0; i<SPD; i++)
		    {
		      VARvarPD.t[i] = 0.;
		      for(int j=0; j<PDIM; j++)
			{
			  VARvarPD.tprime[i][j] = 0.;
			  VARvarPD.dphi[i][j] = 0.;
			}
		    }
		  
		  if( f>=SPD && g>=SPD && a==b )
		    for(int k=0; k<SPD; k++)
		      {
			VARvarPD.t[k] = Shp[nDof*q+a]*VAR_var_nodal_t[a][k][f-SPD][g-SPD];
			for(int p=0; p<PDIM; p++)
			  VARvarPD.tprime[k][p] = DShp[nDof*PDIM*q+PDIM*a+p]*VAR_var_nodal_t[a][k][f-SPD][g-SPD];
		      }
		  
		  // Second variations of strains
		  if( !VARvarSD.Initialize(PD, varPD[f][a], varPD[g][b], VARvarPD) )
		    {
		      std::cerr<<"\nStaticInterpolatedSimoShell3D::GetDVal()- "
			       <<"Could not compute second variations of strains.\n";
		      std::fflush( stdout ); return false;
		    }
		  
		  // Update  dresidue
		  (*dfuncval)[f][a][g][b] += 
		    dA0*VARvarSD.ComputeVarVarVirtualWork(shearstress, memstress, bendstress, 
							  Ashear, Amem, Abend, varSD[f][a], varSD[g][b]);
		}
    }
  return true;
}
