// Sriramajayam

#include "StaticInterpolatedSimoShell.h"
#include "P12DElementNoCoordArray.h"
#include <iomanip>

#define PRINTUPDATES 0
#define PRINTALL 0

void TestOperationUpdate(StaticInterpolatedSimoShell& Op);

void TestOperation(StaticInterpolatedSimoShell& Op);

int main()
{
  // Create shell material
  IsotropicLinearElasticShell3D SMat(1., 0.23, 1.2);
  
  // Create one element
  double elmcoord[] = {1.,0., 0.,1., 0.,0.};
  P12DElementNoCoordArray<5> Elm(1,2,3, elmcoord);
  
  std::vector<int> NodeNum(3);
  for(int a=0; a<3; a++)
    NodeNum[a] = a;
  
  // Vectors of nodal configurations
  std::vector< std::vector<double> > XNodal(3);
  std::vector< std::vector<double> > RNodal(3);
  for(int a=0; a<3; a++)
    {
      XNodal[a].resize(3);
      RNodal[a].resize(9);
    }
  StaticInterpolatedSimoShell::Xn = &XNodal;
  StaticInterpolatedSimoShell::Rn = &RNodal;
  
  StaticInterpolatedSimoShell* clone;
  {
    std::cout<<"\n\nTesting operation: "
	     <<"\n======================";
    double reft[3][3] = {{0.,0.,1.,},{1.,0.,1.},{0.,1.,1.}};
    double refphi[3][3] = {{1.,0.,0.},{0.,1.,0.},{0.,0.,0.}};
    StaticInterpolatedSimoShell Op(&Elm, NodeNum, SMat, refphi, reft);
    TestOperationUpdate(Op); 
    TestOperationUpdate(Op); 
    TestOperation(Op); 
    
    std::cout<<"\n\nTesting copy: "
	     <<"\n================";
    StaticInterpolatedSimoShell copy(Op);
    TestOperation(copy);
    
    clone = copy.Clone();
  }
  
  std::cout<<"\n\nTesting clone: "
	   <<"\n=================";
  TestOperation(*clone);
  delete clone;

  std::cout<<"\n\n===done===\n\n";
  std::fflush( stdout ); 
}


void TestOperationUpdate(StaticInterpolatedSimoShell& Op)
{
  const int ndof = 3;
  const int PDIM = 2;
  std::vector<DResidue*> OpArray(1);
  OpArray[0] = &Op;
  
  if( PRINTUPDATES )
    {
      // Print internal variables at dofs
      std::cout<<"\n\nDof variables prior to update:";
      for(int a=0; a<ndof; a++)
	{
	  std::vector<double> R = Op.GetRotationMatrixAtNode(a);
	  std::vector<double> t = Op.GetSpatialDirectorAtNode(a);
	  std::cout<<"\nDof "<<a<<": "
		   <<"\nR = "
		   <<"\n"<<R[0]<<" "<<R[1]<<" "<<R[2]
		   <<"\n"<<R[3]<<" "<<R[4]<<" "<<R[5]
		   <<"\n"<<R[6]<<" "<<R[7]<<" "<<R[8];
	  std::cout<<"\nt = "<<t[0]<<" "<<t[1]<<" "<<t[2];
	}
    }

  std::vector< std::vector<double> > dX(ndof);
  std::vector< std::vector<double> > dT(ndof);
  for(int a=0; a<ndof; a++)
    {
      dX[a].resize(3);
      dT[a].resize(PDIM);
      for(int i=0; i<3; i++)
	dX[a][i] = 0.;
    }
  
  dT[0][0] = sqrt(0.2); dT[0][1] = sqrt(0.3);
  dT[1][0] = sqrt(0.5); dT[1][1] = sqrt(0.6);
  dT[2][0] = sqrt(0.7); dT[2][1] = sqrt(0.8);
  std::cout<<"\nPerforming Update\n";
  StaticInterpolatedSimoShell::UpdateConfiguration(dX, dT);

  // Print internal variables at dofs
  if( PRINTUPDATES )
    {
      std::cout<<"\n\nDof variables after update:";
      for(int a=0; a<ndof; a++)
	{
	  std::vector<double> R = Op.GetRotationMatrixAtNode(a);
	  std::vector<double> t = Op.GetSpatialDirectorAtNode(a);
	  std::cout<<"\nDof "<<a<<": "
		   <<"\nR = "
		   <<"\n"<<R[0]<<" "<<R[1]<<" "<<R[2]
		   <<"\n"<<R[3]<<" "<<R[4]<<" "<<R[5]
		   <<"\n"<<R[6]<<" "<<R[7]<<" "<<R[8];
	  std::cout<<"\nt = "<<t[0]<<" "<<t[1]<<" "<<t[2];
	}
      std::fflush( stdout );
    }
}



void TestOperation(StaticInterpolatedSimoShell& Op)
{
  const int PDIM = 2;
  const double EPS = 1.e-5;
  const int nFields = 5;
  const int nDof = 3;
  const int SPD = 3;
  
  // Sample parameters
  double dofs[3][3] = {{1.45, 0.32, 0.27},
		       {0.32, 0.96, 0.63},
		       {-0.22, 0.15, 0.53}};
  
  for(int f=0; f<3; f++)
    for(int a=0; a<3; a++)
      (*StaticInterpolatedSimoShell::Xn)[a][f] = dofs[f][a];
  
  // Compute functional:
  std::cout<<"\nTesting calculation of functional for sample parameters: ";
  double F = 0.;
  if( !Op.ComputeFunctional(F) )
    {
      std::cerr<<"\nCould not compute functional.\n";
      std::fflush( stdout ); exit(1);
    }
  std::cout<<"\nFunctional value = "<<F;

  std::vector< std::vector<double> > OrigX = (*StaticInterpolatedSimoShell::Xn);
  std::vector< std::vector<double> > OrigR = (*StaticInterpolatedSimoShell::Rn);
  std::vector< std::vector<double> > dX(nDof), dT(nDof);
  for(int i=0; i<nDof; i++)
    {
      dX[i].resize(SPD);
      dT[i].resize(PDIM);
      for(int j=0; j<SPD; j++)
	dX[i][j] = 0.;
      for(int j=0; j<PDIM; j++)
	dT[i][j] = 0.;
    }
  
  std::cout<<"\nTesting consistency of force vector: ";
  {
    std::vector< std::vector<double> > funcval;
    if( !Op.GetResidual(&funcval) )
      {
	std::cerr<<"\nCould not compute force vector and stiffness matrix.\n";
	std::fflush( stdout ); exit(1);
      }
    
    bool flag = true;
    for(int f=0; f<nFields; f++)
      for(int a=0; a<nDof; a++)
	{
	  for(int i=0; i<nDof; i++)
	    {
	      for(int j=0; j<SPD; j++)
		dX[i][j] = 0.;
	      for(int j=0; j<PDIM; j++)
		dT[i][j] = 0.;
	    }
	  
	  if( f<SPD ) dX[a][f] = EPS;
	  else dT[a][f-SPD] = EPS;
	  StaticInterpolatedSimoShell::UpdateConfiguration(dX, dT);
	  double fplus;
	  Op.ComputeFunctional(fplus);
	  
	  // Restore
	  for(int i=0; i<nDof; i++)
	    {
	      for(int j=0; j<SPD; j++)
		(*StaticInterpolatedSimoShell::Xn)[i][j] = OrigX[i][j];
	      for(int j=0; j<SPD*SPD; j++)
		(*StaticInterpolatedSimoShell::Rn)[i][j] = OrigR[i][j];
	    }
	  
	  if( f<SPD ) dX[a][f] = -EPS;
	  else dT[a][f-SPD] = -EPS;
	  StaticInterpolatedSimoShell::UpdateConfiguration(dX, dT);
	  double fminus;
	  Op.ComputeFunctional(fminus);
	  
	  // Restore
	  for(int i=0; i<nDof; i++)
	    {
	      for(int j=0; j<SPD; j++)
		(*StaticInterpolatedSimoShell::Xn)[i][j] = OrigX[i][j];
	      for(int j=0; j<SPD*SPD; j++)
		(*StaticInterpolatedSimoShell::Rn)[i][j] = OrigR[i][j];
	    }
	  
	  double fnum = (fplus-fminus)/(2.*EPS);
	  if( fabs(funcval[f][a]-fnum)>EPS )
	    {
	      std::cerr<<"\nInconsistency detected: "<<funcval[f][a]<<" should be "<<fnum;
	      flag = false;
	    }
	  if( PRINTALL )
	    std::cout<<"\n"<<funcval[f][a]<<" should be "<<fnum;
	  std::fflush( stdout );
	}
    if( flag==false )
      std::cout<<" FAILED :( ";
    else
      std::cout<<" PASSED :) ";
  }
    
  
  std::cout<<"\nTesting consistency of stiffness: "; std::fflush( stdout );
  {
    std::vector< std::vector<double> > funcval;
    std::vector< std::vector< std::vector< std::vector<double> > > > dfuncval;
    if( !Op.GetJacobian(&funcval, &dfuncval) )
      {
	std::cerr<<"\nCould not compute stiffness.\n";
	std::fflush( stdout ); exit(1);
      }
    
    bool flag = true;
    for(int f=0; f<nFields; f++)
      for(int a=0; a<nDof; a++)
	for(int g=0; g<nFields; g++)
	  for(int b=0; b<nDof; b++)
	    {
	      for(int i=0; i<nDof; i++)
		{
		  for(int j=0; j<SPD; j++)
		    dX[i][j] = 0.;
		  for(int j=0; j<PDIM; j++)
		    dT[i][j] = 0.;
		}
	      
	      if( g<SPD ) dX[b][g] = EPS;
	      else dT[b][g-SPD] = EPS;
	      StaticInterpolatedSimoShell::UpdateConfiguration(dX, dT);
	      Op.GetResidual(&funcval);
	      double fplus = funcval[f][a];

	      
	      // Restore
	      for(int i=0; i<nDof; i++)
		{
		  for(int j=0; j<SPD; j++)
		    (*StaticInterpolatedSimoShell::Xn)[i][j] = OrigX[i][j];
		  for(int j=0; j<SPD*SPD; j++)
		    (*StaticInterpolatedSimoShell::Rn)[i][j] = OrigR[i][j];
		}
	  
	      
	      if( g<SPD ) dX[b][g] = -EPS;
	      else dT[b][g-SPD] = -EPS;
	      StaticInterpolatedSimoShell::UpdateConfiguration(dX, dT);
	      Op.GetResidual(&funcval);
	      double fminus = funcval[f][a];
	      
	      // Restore
	      for(int i=0; i<nDof; i++)
		{
		  for(int j=0; j<SPD; j++)
		    (*StaticInterpolatedSimoShell::Xn)[i][j] = OrigX[i][j];
		  for(int j=0; j<SPD*SPD; j++)
		    (*StaticInterpolatedSimoShell::Rn)[i][j] = OrigR[i][j];
		}
	      
	      double knum = (fplus-fminus)/(2.*EPS);
	      if( fabs(dfuncval[f][a][g][b]-knum)>EPS )
		{
		  std::cerr<<"\nInconsistency detected: "<<dfuncval[f][a][g][b]<<" should be "<<knum;
		  flag  = false;
		}
	      if( PRINTALL )
		std::cout<<"\n"<<dfuncval[f][a][g][b]<<" should be "<<knum;
	      std::fflush( stdout );
	    }
    if( flag==true )
      std::cout<<" PASSED :) ";
    else 
      {
	std::cout<<" FAILED :( ";
	std::fflush( stdout ); exit(1);
      }
  }
  std::fflush( stdout );
}
