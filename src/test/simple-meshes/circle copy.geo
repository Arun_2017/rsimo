rad = 1.0;
Point(1) = {0, 0, 0};
Point(2) = {rad, 0, 0};
Point(3) = {0, rad, 0};
Point(4) = {-rad, 0, 0};
Point(5) = {0, -rad, 0};
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Line(5) = {1,2};
Line(6) = {1,3};
Line Loop(9) = {5, 1, -6};
Plane Surface(10) = {9};
Line Loop(11) = {2, 3, 4, -5, 6};
Plane Surface(12) = {11};
Characteristic Length {1} = 0.01;
Characteristic Length {2} = 0.1;
Characteristic Length {3} = 0.09;
Characteristic Length {4} = 0.08;
Characteristic Length {5} = 0.075;
