// Sriramajayam

#include "MeshUtils.h"
#include "PlottingUtils.h"

const char FLDR[] = "data/unconstrained-one";

int main()
{
  CoordConn MD;
  MD.nodes_element = 3;
  MD.spatial_dimension = 3;
  char filename[100];
  sprintf(filename, "%s/ref.tec", FLDR);
  ReadTecplotFile(filename, MD);
  
  // Identify nodes along y = 0
  const double EPS = 1.e-4;
  std::map<double, int> ymap; ymap.clear();
  for(int n=0; n<MD.nodes; n++)
    if( fabs(MD.coordinates[3*n+1])<EPS )
      ymap[ MD.coordinates[3*n] ] = n;
  
  for(int f=0; f<370; f++)
    {
      sprintf(filename, "%s/z-%03d.dat", FLDR, f);
      std::fstream zfile;
      zfile.open(filename, std::ios::out);
      
      sprintf(filename, "%s/def-%03d.tec", FLDR, f);
      std::cout<<"\nReading file "<<filename; std::fflush( stdout );
      CoordConn DefMD;
      DefMD.spatial_dimension = 3;
      DefMD.nodes_element = 3;
      ReadTecplotFile(filename, DefMD);
      int num = int(ymap.size());
      int j = 0;
      for(std::map<double, int>::const_iterator it=ymap.begin(); it!=ymap.end() && j<num-1; it++, j++)
	{
	  double x = it->first;
	  int n = it->second;
	  double z = DefMD.coordinates[3*n+2];
	  
	  std::map<double, int>::const_iterator itnext = it; itnext++;
	  double xnext = itnext->first;
	  int nnext = itnext->second;
	  double znext = DefMD.coordinates[3*nnext+2];
	  double zprime = (znext-z)/(xnext-x);
	  
	  zfile << x << "\t" << z <<"\t"<<zprime<<"\n"; zfile.flush();
	}
      zfile.close();
    }
}
