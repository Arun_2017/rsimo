// Sriramajayam

#include "P12DElementNoCoordArray.h"
#include "StaticInterpolatedSimoShell.h"
#include "MeshUtils.h"
#include "PlottingUtils.h"
#include "TriangleSubdivision.h"

const int PDIM = 2;
const int nFields = 5;
const int SPD = 3;
const int nDof = 3;

char OUTFLDR[] = "unconstrained";

// Identify boundary conditions
void GetDirichletBCs(const CoordConn& MD, const double theta, 
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Compute the energy of a solution
void ComputeStrainEnergies(const std::vector<DResidue*>& OpArray, 
			   const CoordConn& MD, 
			   const double* phi, 
			   double& Etotal, double& Emem, double &Ebend, double& Eshear);

// Compute the total energy of a solution
double ComputeTotalStrainEnergy(const std::vector<DResidue*>& OpArray, 
				const CoordConn& MD,
				const double* phi);

// Computes the sum of the out-of-plane displacement of nodes along the central axis
double ComputeNormOfOutOfPlaneDisplacement(const CoordConn& MD, const double* phi);

int main(int argc, char** argv)
{
  // Read 3D mesh
  CoordConn MD;
  MD.spatial_dimension = SPD;
  MD.nodes_element = 3;
  ReadTecplotFile((char*)"meshes/Strip.msh", MD);
  SubdivideTriangles<SPD>(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  char filename[100];
  sprintf(filename, "%s/ref.tec", OUTFLDR);
  PlotTecCoordConn(filename, MD);


  // Create elements
  const double UnitTriangle[] = {1.,0., 0.,1., 0.,0.};
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; e++)
    ElmArray[e] = new P12DElementNoCoordArray<nFields>
      (MD.connectivity[3*e], MD.connectivity[3*e+1], MD.connectivity[3*e+2], UnitTriangle);
  
  // Local to global map
  StandardP12DMap L2GMap(ElmArray);
  
  // Create shell material
  const double YM = 1.;
  const double nu = 0.0;//25;
  const double thickness = 0.1;
  const double rho = 1.;
  IsotropicLinearElasticShell3D SMat(YM, nu, thickness, rho);
  
  // Global rotations array
  std::vector< std::vector<double> > RNodal(MD.nodes);
  for(int n=0; n<MD.nodes; n++)
    RNodal[n].resize( SPD*SPD );
  
  // Create operations
  StaticInterpolatedSimoShell::SetNodalRotationsArray(RNodal);
  std::vector<DResidue*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; e++)
    {
      double reft[][SPD] = {{0.,0.,1.},{0.,0.,1.},{0.,0.,1.}};
      double refPhi[MD.nodes_element][SPD];
      std::vector<int> NodeNum(MD.nodes_element);
      for(int a=0; a<MD.nodes_element; a++)
	{
	  int n = MD.connectivity[3*e+a]-1;
	  NodeNum[a] = n;
	  for(int k=0; k<SPD; k++)
	    refPhi[a][k] = MD.coordinates[SPD*n+k];
	}
      OpArray[e] = new StaticInterpolatedSimoShell(ElmArray[e], NodeNum, SMat, refPhi, reft);
    }

  // Set initial guess for mid-surface coordinates
  std::vector<double> Phi = MD.coordinates;

  // Space for material director increments
  std::vector< std::vector<double> > dT(MD.nodes);
  for(int n=0; n<MD.nodes; n++)
    dT[n].resize(PDIM);
  
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create PETSc data structire
  PetscData PD;
  InitializePetscData(L2GMap, L2GMap.GetTotalNumDof(), PD);
  
  // Set initial guess for dofs
  VecSet(PD.DOFArray, 0.);
  for(int n=0; n<MD.nodes; n++)
    {
      int indx[] = {nFields*n, nFields*n+1, nFields*n+2};
      VecSetValues(PD.DOFArray, SPD, indx, &Phi[SPD*n], INSERT_VALUES);
    }
  VecAssemblyBegin(PD.DOFArray);
  VecAssemblyEnd(PD.DOFArray);
  
  // Random number generator
  PetscRandom rnd;
  PetscRandomCreate(PETSC_COMM_WORLD, &rnd);
  PetscRandomSetType(rnd,PETSCRAND48);
  PetscRandomSetInterval(rnd, -0.001, 0.001);

  // Track the energy
  std::fstream efile;
  sprintf(filename, "%s/energy.dat", OUTFLDR);
  efile.open(filename, std::ios::out);
  
  // Load stepping
  for(int step=0; step<600; step++)
    {
      std::cout<<"\n\nLoading step "<<step+1
	       <<"\n===================\n";
      std::fflush( stdout );
      
      // Get Dirichlet BCs
      double theta = 1.*double(step)*M_PI/180.;  // degree increments
      std::vector<int> boundary;
      std::vector<double> bvalues;
      GetDirichletBCs(MD, theta, boundary, bvalues);

      // Add a small perturbation to the solution guess
      /*for(int n=0; n<MD.nodes; n++)
	{
	  int msindex[] = {nFields*n, nFields*n+1, nFields*n+2};
	  double pert[SPD];
	  for(int k=0; k<SPD; k++)
	    PetscRandomGetValueReal(rnd, &pert[k]);
	  VecSetValues(PD.DOFArray, SPD, msindex, pert, ADD_VALUES);
	}
      VecAssemblyBegin(PD.DOFArray);
      VecAssemblyEnd(PD.DOFArray);*/
      
      // Newton Raphson
      int iter = 0.;
      while(true)
	{
	  std::cout<<"Iteration "<<++iter<<": "; std::fflush( stdout );

	  // Assemble matrix-vector system
	  VecSet(PD.resVEC, 0.);
	  VecSet(PD.solutionVEC, 0.);
	  if( !DResidue::Assemble(OpArray, L2GMap, PD.DOFArray, 
				  &PD.resVEC, &PD.stiffnessMAT) )
	    {
	      std::cerr<<"\nCould not assemble matrix-vector system.\n";
	      std::fflush( stdout ); exit(1);
	    }

	  // Set Dirichlet BCs
	  SetDirichletBCs(PD, boundary, bvalues);

	  // Solve
	  Solve(PD);
	  
	  // Check convergence
	  if( converged(PD, 1.e-10, 1., 1.) )
	    { std::cout<<"\nconverged!\n"; std::fflush( stdout ); break; }

	  // Reverse the sign of the solution
	  VecScale(PD.solutionVEC, -1.);
	  
	  // Update using computed increments
	  for(int n=0; n<MD.nodes; n++)
	    {
	      int msindex[] = {nFields*n, nFields*n+1, nFields*n+2};
	      double dPhi[SPD];
	      VecGetValues(PD.solutionVEC, SPD, msindex, dPhi);
	      for(int k=0; k<SPD; k++)
		Phi[SPD*n+k] += dPhi[k];
	      VecSetValues(PD.DOFArray, SPD, msindex, &Phi[SPD*n], INSERT_VALUES);
	      
	      int tindex[] = {nFields*n+SPD, nFields*n+SPD+1};
	      VecGetValues(PD.solutionVEC, PDIM, tindex, &dT[n][0]);
	    }
	  VecAssemblyBegin(PD.DOFArray);
	  VecAssemblyEnd(PD.DOFArray);
	  if( !StaticInterpolatedSimoShell::UpdateDirectorField(dT) )
	    {
	      std::cerr<<"\nCould not update spatial directors.\n";
	      std::fflush( stdout ); exit(1);
	    }
	}

      // Plot the mid-surface deformations
      CoordConn DefMD = MD;
      DefMD.coordinates = Phi;
      sprintf(filename, "%s/def-%03d.tec", OUTFLDR, step);
      PlotTecCoordConn(filename, DefMD);

      // Plot directors
      double spdir[SPD*MD.nodes];
      if( !StaticInterpolatedSimoShell::GetNodalSpatialDirectors(spdir) )
	{
	  std::cerr<<"\nCould not compute nodal director field.\n";
	  std::fflush( stdout ); exit(1);
	}
      sprintf(filename, "%s/t-%03d.tec", OUTFLDR, step);
      PlotTecCoordConnWithNodalFields(filename, DefMD, spdir, SPD);

      // Compute the energy of a solution
      double Etotal = 0., Emem = 0., Eshear = 0., Ebend = 0.;
      ComputeStrainEnergies(OpArray, MD, &Phi[0], Etotal, Emem, Ebend, Eshear);
      
      // Out-of-plane displacement
      double Znorm = ComputeNormOfOutOfPlaneDisplacement(MD, &Phi[0]);

      // Print these values
      efile << 1.*double(step)*M_PI/180. <<" "<<Znorm<<" "
	    <<Etotal<<" "<<Emem<<" "<<Ebend<<" "<<Eshear<<"\n"; efile.flush();
    }

  // Clean up
  DestroyPetscData(PD);
  for(int e=0; e<MD.elements; e++)
    {
      delete ElmArray[e];
      delete OpArray[e];
    }
  PetscRandomDestroy(&rnd);
  PetscFinalize();
  std::cout<<"\n\n===done===\n\n"; std::fflush( stdout );
}
		
	
// Identify boundary conditions
void GetDirichletBCs(const CoordConn& MD, const double theta, 
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  boundary.clear();
  bvalues.clear();

  // Get element neighbor list
  std::vector< std::vector<int> > ElmNbs;
  ElmNbs.clear();
  GetCoordConnFaceNeighborList(MD, ElmNbs);

  const double EPS = 1.e-4;
  
  // Rotation matrix
  const double RotMat[][SPD] = {{1.,0.,0.},
				{0.,cos(theta), -sin(theta)},
				{0.,sin(theta), cos(theta)}};
  
  for(int e=0; e<MD.elements; e++)
    for(int face=0; face<3; face++)
      // Is this a free face
      if( ElmNbs[e][2*face]<0 )
	{
	  double coord[2][SPD];
	  int nodenum[2];
	  for(int j=0; j<2; j++)
	    {
	      int n = MD.connectivity[3*e+(face+j)%3]-1;
	      nodenum[j] = n;
	      for(int k=0; k<SPD; k++)
		coord[j][k] = MD.coordinates[SPD*n+k];
	    }
	  
	  // Clamp the face x = 10
	  if( fabs(coord[0][0]-10.)+fabs(coord[1][0]-10.)<EPS )
	    for(int a=0; a<2; a++)
	      {
		for(int f=0; f<SPD; f++)
		  {
		    boundary.push_back( nFields*nodenum[a]+f );
		    bvalues.push_back( coord[a][f] );
		  }
		for(int f=SPD; f<SPD+PDIM; f++)
		  {
		    boundary.push_back( nFields*nodenum[a]+f );
		    bvalues.push_back( 0. );
		  }
	      }
	  // Twist the face x = 0
	  else if( fabs(coord[0][0])+fabs(coord[1][0])<EPS )
	    {
	      // Transform coordinates
	      double newcoord[2][SPD];
	      for(int a=0; a<2; a++)
		for(int i=0; i<SPD; i++)
		  {
		    newcoord[a][i] = 0.;
		    for(int j=0; j<SPD; j++)
		      newcoord[a][i] += RotMat[i][j]*coord[a][j];
		  }
	      
	      // Don't constrain the x-displacement
	      for(int a=0; a<2; a++)
		for(int f=1; f<SPD; f++)
		  {
		    boundary.push_back( nFields*nodenum[a]+f );
		    bvalues.push_back( newcoord[a][f] );
		  }
	    }
	}
}




// Compute the energy of a solution
void ComputeStrainEnergies(const std::vector<DResidue*>& OpArray, 
			   const CoordConn& MD, 
			   const double* phi, 
			   double& Etotal, double& Emem, double &Ebend, double& Eshear)
{
  // Get the material
  const IsotropicLinearElasticShell3D* SMat = 
    dynamic_cast<const StaticInterpolatedSimoShell*>(OpArray[0])->GetShellMaterial();
  
  // Create materials to evaluate components of strain energy
  IsotropicLinearElasticShell3D* MemMat = SMat->Clone();
  MemMat->SetBendingModulus(0.);
  MemMat->SetShearModulus(0.);

  IsotropicLinearElasticShell3D* BendMat = SMat->Clone();
  BendMat->SetMembraneModulus(0.);
  BendMat->SetShearModulus(0.);

  IsotropicLinearElasticShell3D* ShearMat = SMat->Clone();
  ShearMat->SetMembraneModulus(0.);
  ShearMat->SetBendingModulus(0.);

  // Initialize energies
  Etotal = 0.;
  Emem = 0.;
  Ebend = 0.;
  Eshear = 0.;

  const int nElements = int(OpArray.size());
  std::vector< std::vector<double> > argval(nFields);
  for(int f=0; f<nFields; f++)
    {
      argval[f].resize(3);
      for(int a=0; a<3; a++)
	argval[f][a] = 0.;
    }
  
  for(int e=0; e<nElements; e++)
    {
      const StaticInterpolatedSimoShell* Op = 
	dynamic_cast<const StaticInterpolatedSimoShell*>(OpArray[e]);
      if( Op==NULL )
	{
	  std::cerr<<"\nComputeStrainEnergies()- Could not typecast operation.\n";
	  std::fflush( stdout ); exit(1);
	}
      
      // Assign values of dofs
      for(int a=0; a<3; a++)
	{
	  int n = MD.connectivity[3*e+a]-1;
	  for(int f=0; f<SPD; f++)
	    argval[f][a] = phi[SPD*n+f];
	}

      // Compute the contribution from this operation
      double dTotal = 0.;
      if( !Op->ComputeFunctional(argval, dTotal) )
	{
	  std::cerr<<"\nComputeStrainEnergies()- Could not compute total energy contribution.\n";
	  std::fflush( stdout ); exit(1);
	}
      Etotal += dTotal;
      
      // Compute components of the strain energy
      StaticInterpolatedSimoShell* Copy = Op->Clone();
      
      // Membrane contribution
      Copy->SetShellMaterial(*MemMat);
      double dMem = 0.;
      Copy->ComputeFunctional(argval, dMem);
      Emem += dMem;
      
      // Shear contribution
      Copy->SetShellMaterial(*ShearMat);
      double dShear = 0.;
      Copy->ComputeFunctional(argval, dShear);
      Eshear += dShear;
      
      // Bending contribution
      Copy->SetShellMaterial(*BendMat);
      double dBend = 0.;
      Copy->ComputeFunctional(argval, dBend);
      Ebend += dBend;
      
      delete Copy;

      // Consistency check
      if( fabs(dTotal-dBend-dShear-dMem)>1.e-12 )
	{
	  std::cerr<<"\nComputeStrainEnergies()- "
		   <<"Inconsistency in computed contributions to the strain energy.\n";
	  std::fflush( stdout ); exit(1);
	}
    }

  delete ShearMat;
  delete MemMat;
  delete BendMat;
}



      
// Compute the total energy of a solution
double ComputeTotalStrainEnergy(const std::vector<DResidue*>& OpArray, 
				const CoordConn& MD,
				const double* phi)
{
  double W = 0.;
  const int nElements = int(OpArray.size());
  std::vector< std::vector<double> > argval(nFields);
  for(int f=0; f<nFields; f++)
    {
      argval[f].resize(3);
      for(int a=0; a<3; a++)
	argval[f][a] = 0.;
    }
  
  for(int e=0; e<nElements; e++)
    {
      const StaticInterpolatedSimoShell* Op = 
	dynamic_cast<const StaticInterpolatedSimoShell*>(OpArray[e]);
      if( Op==NULL )
	{
	  std::cerr<<"\nComputeStrainEnergy()- Could not type cast object.\n";
	  std::fflush( stdout ); return 0.;
	}
      
      for(int a=0; a<3; a++)
	{
	  int n = MD.connectivity[3*e+a]-1;
	  for(int f=0; f<SPD; f++)
	    argval[f][a] = phi[SPD*n+f];
	}
      
      double dW = 0.;
      if( !Op->ComputeFunctional(argval, dW) )
	{
	  std::cerr<<"\nComputeStrainEnergy()- Could not compute contribution from element.\n";
	  std::fflush( stdout ); return 0.;
	}
      
      W += dW;
    }
  return W;
}


// Computes the sum of the out-of-plane displacement of nodes along the central axis
double ComputeNormOfOutOfPlaneDisplacement(const CoordConn& MD, const double* phi)
{
  double Zmax = 0.;
  const double EPS = 1.e-4;
  for(int n=0; n<MD.nodes; n++)
    if( fabs(MD.coordinates[SPD*n+1])<EPS )
      if( fabs(phi[SPD*n+2])>Zmax )
	Zmax = fabs(phi[SPD*n+2]);
  return Zmax;
}
