// Sriramajayam


#include "P12DElementNoCoordArray.h"
#include "StaticInterpolatedSimoShell.h"
#include "MeshUtils.h"
#include "PlottingUtils.h"
#include "TriangleSubdivision.h"

const int PDIM = 2;
const int nFields = 5;
const int SPD = 3;
const int nDof = 3;


// Identify boundary conditions and constraints
void GetDirichletBCs(const CoordConn& MD, const double theta, 
		     std::vector<int>& boundary, std::vector<double>& bvalues);

// Identify constraints to be imposed
void GetDOFConstraints(const CoordConn& MD, 
		       std::map<int, int>& constraints);

// Set DOF constraints
void SetDOFConstraints(Mat& K, Vec& R, const std::map<int, int>& constraints);

const char OUTFLDR[] = ".";

int main(int argc, char** argv)
{
  // Read 3D mesh
  CoordConn MD;
  MD.spatial_dimension = SPD;
  MD.nodes_element = 3;
  ReadTecplotFile((char*)"meshes/Strip.msh", MD);
  SubdivideTriangles<SPD>(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  char filename[100];
  sprintf(filename, "%s/ref.tec", OUTFLDR);
  PlotTecCoordConn(filename, MD);
  
  // Create elements
  const double UnitTriangle[] = {1.,0., 0.,1., 0.,0.};
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; e++)
    ElmArray[e] = new P12DElementNoCoordArray<nFields>
      (MD.connectivity[3*e], MD.connectivity[3*e+1], MD.connectivity[3*e+2], UnitTriangle);
  
  // Local to global map
  StandardP12DMap L2GMap(ElmArray);
  
  // Create shell material
  const double YM = 1.;
  const double nu = 0.0;
  const double thickness = 0.05;
  const double rho = 1.;
  IsotropicLinearElasticShell3D SMat(YM, nu, thickness, rho);
  
  
    // Global rotations array
  std::vector< std::vector<double> > RNodal(MD.nodes);
  for(int n=0; n<MD.nodes; n++)
    RNodal[n].resize( SPD*SPD );
  
  // Create operations
  StaticInterpolatedSimoShell::SetNodalRotationsArray(RNodal);
  std::vector<DResidue*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; e++)
    {
      double reft[][SPD] = {{0.,0.,1.},{0.,0.,1.},{0.,0.,1.}};
      double refPhi[MD.nodes_element][SPD];
      std::vector<int> NodeNum(MD.nodes_element);
      for(int a=0; a<MD.nodes_element; a++)
	{
	  int n = MD.connectivity[3*e+a]-1;
	  NodeNum[a] = n;
	  for(int k=0; k<SPD; k++)
	    refPhi[a][k] = MD.coordinates[SPD*n+k];
	}
      OpArray[e] = new StaticInterpolatedSimoShell(ElmArray[e], NodeNum, SMat, refPhi, reft);
    }


  // Set initial guess for mid-surface coordinates
  std::vector<double> Phi = MD.coordinates;

  // Space for material director increments
  std::vector< std::vector<double> > dT(MD.nodes);
  for(int n=0; n<MD.nodes; n++)
    dT[n].resize(PDIM);
  
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  // Create PETSc data structire
  PetscData PD;
  InitializePetscData(L2GMap, L2GMap.GetTotalNumDof(), PD);
  MatSetOption(PD.stiffnessMAT, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);
  
  // Set initial guess for dofs
  VecSet(PD.DOFArray, 0.);
  for(int n=0; n<MD.nodes; n++)
    {
      int indx[] = {nFields*n, nFields*n+1, nFields*n+2};
      VecSetValues(PD.DOFArray, SPD, indx, &Phi[SPD*n], INSERT_VALUES);
    }
  VecAssemblyBegin(PD.DOFArray);
  VecAssemblyEnd(PD.DOFArray);
  
  // Identify the set of constraints to be imposed
  std::map<int, int> constraints;
  GetDOFConstraints(MD, constraints);
  
  // Load stepping
  for(int step=0; step<400; step++)
    {
      std::cout<<"\n\nLoading step "<<step+1
	       <<"\n===================\n";
      std::fflush( stdout );
      
      // Get Dirichlet BCs and constraints
       double theta = 1.*double(step)*M_PI/180.;  // degree increments
      std::vector<int> boundary;
      std::vector<double> bvalues;
      GetDirichletBCs(MD, theta, boundary, bvalues);
  
       // Newton Raphson
      int iter = 0.;
      while(true)
	{
	  std::cout<<"Iteration "<<++iter<<": "; std::fflush( stdout );

	  // Assemble matrix-vector system
	  VecSet(PD.resVEC, 0.);
	  VecSet(PD.solutionVEC, 0.);
	  if( !DResidue::Assemble(OpArray, L2GMap, PD.DOFArray, 
				  &PD.resVEC, &PD.stiffnessMAT) )
	    {
	      std::cerr<<"\nCould not assemble matrix-vector system.\n";
	      std::fflush( stdout ); exit(1);
	    }

	  // Set Dirichlet BCs
	  SetDirichletBCs(PD, boundary, bvalues);
	  
	  // Set DOF constraints
	  SetDOFConstraints(PD.stiffnessMAT, PD.resVEC, constraints);
	  
	  // Solve
	  Solve(PD);
	  
	  // Check convergence
	  if( converged(PD, 1.e-10, 1., 1.) )
	    { std::cout<<"\nconverged!\n"; std::fflush( stdout ); break; }

	  // Reverse the sign of the solution
	  VecScale(PD.solutionVEC, -1.);
	  
	  // Update using computed increments
	  for(int n=0; n<MD.nodes; n++)
	    {
	      int msindex[] = {nFields*n, nFields*n+1, nFields*n+2};
	      double dPhi[SPD];
	      VecGetValues(PD.solutionVEC, SPD, msindex, dPhi);
	      for(int k=0; k<SPD; k++)
		Phi[SPD*n+k] += dPhi[k];
	      VecSetValues(PD.DOFArray, SPD, msindex, &Phi[SPD*n], INSERT_VALUES);
	      
	      int tindex[] = {nFields*n+SPD, nFields*n+SPD+1};
	      VecGetValues(PD.solutionVEC, PDIM, tindex, &dT[n][0]);
	    }
	  VecAssemblyBegin(PD.DOFArray);
	  VecAssemblyEnd(PD.DOFArray);
	  if( !StaticInterpolatedSimoShell::UpdateDirectorField(dT) )
	    {
	      std::cerr<<"\nCould not update spatial directors.\n";
	      std::fflush( stdout ); exit(1);
	    }
	}

       // Plot the mid-surface deformations
      CoordConn DefMD = MD;
      DefMD.coordinates = Phi;
      sprintf(filename, "%s/def-%03d.tec", OUTFLDR, step);
      PlotTecCoordConn(filename, DefMD);

      // Plot directors
      double spdir[SPD*MD.nodes];
      if( !StaticInterpolatedSimoShell::GetNodalSpatialDirectors(spdir) )
	{
	  std::cerr<<"\nCould not compute nodal director field.\n";
	  std::fflush( stdout ); exit(1);
	}
      sprintf(filename, "%s/t-%03d.tec", OUTFLDR, step);
      PlotTecCoordConnWithNodalFields(filename, DefMD, spdir, SPD);

       }

  // Clean up
  DestroyPetscData(PD);
  for(int e=0; e<MD.elements; e++)
    {
      delete ElmArray[e];
      delete OpArray[e];
    }
  PetscFinalize();
  std::cout<<"\n\n===done===\n\n"; std::fflush( stdout );
}


// Identify boundary conditions
void GetDirichletBCs(const CoordConn& MD, const double theta, 
		     std::vector<int>& boundary, std::vector<double>& bvalues)
{
  boundary.clear();
  bvalues.clear();

  // Get element neighbor list
  std::vector< std::vector<int> > ElmNbs;
  ElmNbs.clear();
  GetCoordConnFaceNeighborList(MD, ElmNbs);

  const double EPS = 1.e-4;
  
  // Rotation matrix
  const double RotMat[][SPD] = {{1.,0.,0.},
				{0.,cos(theta), -sin(theta)},
				{0.,sin(theta), cos(theta)}};
  
  for(int e=0; e<MD.elements; e++)
    for(int face=0; face<3; face++)
      // Is this a free face
      if( ElmNbs[e][2*face]<0 )
	{
	  double coord[2][SPD];
	  int nodenum[2];
	  for(int j=0; j<2; j++)
	    {
	      int n = MD.connectivity[3*e+(face+j)%3]-1;
	      nodenum[j] = n;
	      for(int k=0; k<SPD; k++)
		coord[j][k] = MD.coordinates[SPD*n+k];
	    }
	  
	  // Clamp the face x = 10
	  if( fabs(coord[0][0]-10.)+fabs(coord[1][0]-10.)<EPS )
	    for(int a=0; a<2; a++)
	      {
		for(int f=0; f<SPD; f++)
		  {
		    boundary.push_back( nFields*nodenum[a]+f );
		    bvalues.push_back( coord[a][f] );
		  }
		for(int f=SPD; f<SPD+PDIM; f++)
		  {
		    boundary.push_back( nFields*nodenum[a]+f );
		    bvalues.push_back( 0. );
		  }
	      }
	  // Twist the face x = 0
	  else if( fabs(coord[0][0])+fabs(coord[1][0])<EPS )
	    {
	      // Transform coordinates
	      double newcoord[2][SPD];
	      for(int a=0; a<2; a++)
		for(int i=0; i<SPD; i++)
		  {
		    newcoord[a][i] = 0.;
		    for(int j=0; j<SPD; j++)
		      newcoord[a][i] += RotMat[i][j]*coord[a][j];
		  }
	      
	      // Don't constrain the x-displacement
	      for(int a=0; a<2; a++)
		for(int f=1; f<SPD; f++)
		  {
		    boundary.push_back( nFields*nodenum[a]+f );
		    bvalues.push_back( newcoord[a][f] );
		  }
	    }
	}
}


// Identify constraints to be imposed
void GetDOFConstraints(const CoordConn& MD, 
		       std::map<int, int>& constraints)
{
  // Identify the nodes lying on the face x = 0
  // and the node at x=0, y=0
  std::set<int> nodes; nodes.clear();
  int cnode = -1;
  double EPS = 1.e-4;
  for(int n=0; n<MD.nodes; n++)
    if( fabs(MD.coordinates[SPD*n]-0.)<EPS )
      {
	if( fabs(MD.coordinates[SPD*n+1]-0.)<EPS )
	  cnode = n;
	else
	  nodes.insert( n );
      }
  if( cnode<0 )
    {
      std::cerr<<"\nGetDOFConstraints()- Could not find center node.\n";
      std::fflush( stdout ); exit(1);
    }
  if( nodes.find(cnode)!=nodes.end() )
    {
      std::cerr<<"\nInconsistency found in set of constraint nodes.\n";
      std::fflush( stdout ); exit(1);
    }
  
  // Set single value of x displacements and rotations for the face x = 0
  constraints.clear();
  for(std::set<int>::const_iterator it=nodes.begin(); it!=nodes.end(); it++)
    {
      int n = *it;
      constraints[ nFields*n+0 ]     = nFields*cnode+0;
      constraints[ nFields*n+SPD ]   = nFields*cnode+SPD;
      constraints[ nFields*n+SPD+1 ] = nFields*cnode+SPD+1;
    }
}



// Set DOF constraints
void SetDOFConstraints(Mat& K, Vec& R, const std::map<int, int>& constraints)
{
  // Finish assembly
  MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);
  VecAssemblyBegin(R);
  VecAssemblyEnd(R);
  
  // Identify the rows to zero out
  std::vector<int> rows; rows.clear();
  for(std::map<int, int>::const_iterator it=constraints.begin(); it!=constraints.end(); it++)
    rows.push_back( it->first );
  MatZeroRows(K, int(rows.size()), &rows[0], 1., PETSC_NULL, PETSC_NULL);
  
  for(std::map<int, int>::const_iterator it=constraints.begin(); it!=constraints.end(); it++)
    {
      int row = it->first;
      int col = it->second;
      double mone = -1.;
      MatSetValues(K, 1, &row, 1, &col, &mone, INSERT_VALUES);
      double zero = 0.;
      VecSetValues(R, 1, &row, &zero, INSERT_VALUES);
    }

  // Finish assembly
  MatAssemblyBegin(K, MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(K, MAT_FINAL_ASSEMBLY);
  VecAssemblyBegin(R);
  VecAssemblyEnd(R);
}
