r = 1.0;
L = 25.0;
Point(1) = {0, 0, 0};
Point(2) = {r, 0, 0};
Point(3) = {0, r, 0};
Point(4) = {-r, 0, 0};
Point(5) = {0, -r, 0};
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Line Loop(1) = {1,2,3,4};
Plane Surface(5) = {1};
Extrude {0, 0, L} { Surface{5}; }
Physical Surface(28) = {22, 26, 14, 18};
Delete { Volume{1}; }
Delete { Surface{27}; }
Delete { Surface{5}; }
N = 10;
M = 75;
Transfinite Line {1,2,3,4,7,8,9,10} = N Using Progression 1;
Transfinite Line {12,13,17,21} = M Using Progression 1;