// Sriramajayam

#include "SimoShellStructs.h"

using namespace SimoShell;

// Reference strains ( at quadarture points )
//=============================================
ReferenceStrains::ReferenceStrains()
{
  for(int i=0; i<2; i++)
    {
      ref_shearstrain[i] = 0.;
      for(int j=0; j<2; j++)
	{
	  ref_memstrain[i][j] = 0.;
	  ref_bendstrain[i][j] = 0.;
	  ginv[i][j] = 0.;
	}
    }
}


ReferenceStrains::ReferenceStrains(const ReferenceStrains& Obj)
{
  for(int i=0; i<2; i++)
    {
      ref_shearstrain[i] = Obj.ref_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  ref_memstrain[i][j] = Obj.ref_memstrain[i][j];
	  ref_bendstrain[i][j] = Obj.ref_bendstrain[i][j];
	  ginv[i][j] = Obj.ginv[i][j];
	}
    }
}


ReferenceStrains& ReferenceStrains::operator=(const ReferenceStrains& rhs)
{
  if( this==&rhs )
    return *this;

  for(int i=0; i<2; i++)
    {
      ref_shearstrain[i] = rhs.ref_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  ref_memstrain[i][j] = rhs.ref_memstrain[i][j];
	  ref_bendstrain[i][j] = rhs.ref_bendstrain[i][j];
	  ginv[i][j] = rhs.ginv[i][j];
	}
    }
  return *this;
}



bool ReferenceStrains::Initialize(const PointData& PD)
{
  double g[2][2];
  for(int i=0; i<2; i++)
    {
      ref_shearstrain[i] = 0.;
      for(int k=0; k<3; k++)
	ref_shearstrain[i] += PD.dphi[k][i]*PD.t[k];
      
      for(int j=0; j<2; j++)
	{
	  g[i][j] = 0.;
	  ref_memstrain[i][j] = 0.;
	  ref_bendstrain[i][j] = 0.;
	  for(int k=0; k<3; k++)
	    {
	      g[i][j] += PD.dphi[k][i]*PD.dphi[k][j];
	      ref_memstrain[i][j] += 0.5*PD.dphi[k][i]*PD.dphi[k][j];
	      ref_bendstrain[i][j] += 0.5*( PD.dphi[k][i]*PD.tprime[k][j] + 
					    PD.dphi[k][j]*PD.tprime[k][i] );
	    }
	}
    }
  
  // Inverse of metric tensor
  double detg = g[0][0]*g[1][1]-g[0][1]*g[1][0];
  if( detg<1.e-12 )
    {
      std::cerr<<"\nSimoShell:ReferenceStrains::Initialize()- "
	       <<"Metric tensor is not invertible at quadrature point.\n";
      std::fflush( stdout ); return false;
    }
  ginv[0][0] = g[1][1]/detg;
  ginv[1][1] = g[0][0]/detg;
  ginv[0][1] = -g[0][1]/detg;
  ginv[1][0] = -g[1][0]/detg;
  
  return true;
}

// Point data (midsurface coordinates and  director information)
// =============================================================
PointData::PointData()
{
  for(int i=0; i<3; i++)
    {
      t[i] = 0.;
      for(int j=0; j<2; j++)
	{
	  dphi[i][j] = 0.;
	  tprime[i][j] = 0.;
	}
    }
}



PointData::PointData(const PointData& Obj)
{
  for(int i=0; i<3; i++)
    {
      t[i] = Obj.t[i];
      for(int j=0; j<2; j++)
	{
	  dphi[i][j] = Obj.dphi[i][j];
	  tprime[i][j] = Obj.tprime[i][j];
	}
    }
}


PointData& PointData::operator=(const PointData& rhs)
{
  if( this==&rhs )
    return *this;
 
  for(int i=0; i<3; i++)
    {
      t[i] = rhs.t[i];
      for(int j=0; j<2; j++)
	{
	  dphi[i][j] = rhs.dphi[i][j];
	  tprime[i][j] = rhs.tprime[i][j];
	}
    }
  return *this;
}




// Data for strain calculations
// =================================
StrainData::StrainData()
{
  for(int i=0; i<2; i++)
    {
      shearstrain[i] = 0.;
      for(int j=0; j<2; j++)
	{
	  memstrain[i][j] = 0.;
	  bendstrain[i][j] = 0.;
	}
    }
}


StrainData::StrainData(const StrainData& Obj)
{
  for(int i=0; i<2; i++)
    {
      shearstrain[i] = Obj.shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  memstrain[i][j] = Obj.memstrain[i][j];
	  bendstrain[i][j] = Obj.bendstrain[i][j];
	}
    }
}


StrainData& StrainData::operator=(const StrainData& rhs)
{
  if( this==&rhs )
    return *this;
  
  for(int i=0; i<2; i++)
    {
      shearstrain[i] = rhs.shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  memstrain[i][j] = rhs.memstrain[i][j];
	  bendstrain[i][j] = rhs.bendstrain[i][j];
	}
    }
  return *this;
}


bool StrainData::Initialize(const ReferenceStrains& RS, const PointData& PD)
{
  // Compute strains at this point
  for(int i=0; i<2; i++)
    {
      shearstrain[i] = -RS.ref_shearstrain[i];
      for(int k=0; k<3; k++)
	shearstrain[i] += PD.dphi[k][i]*PD.t[k];
      
      for(int j=0; j<2; j++)
	{
	  memstrain[i][j] = -RS.ref_memstrain[i][j];
	  bendstrain[i][j] = -RS.ref_bendstrain[i][j];
	  for(int k=0; k<3; k++)
	    {
	      memstrain[i][j] += 0.5*PD.dphi[k][i]*PD.dphi[k][j];
	      bendstrain[i][j] += 0.5*( PD.dphi[k][i]*PD.tprime[k][j] + 
					PD.dphi[k][j]*PD.tprime[k][i] );
	    }
	}
    }
  
  return true;
}


    
double StrainData::ComputeVirtualWork(const double* shearstress, 
				      const double memstress[][2], 
				      const double bendstress[][2]) const
{
  double W = 0.;
  for(int i=0; i<2; i++)
    {
      W += 0.5*shearstress[i]*shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  W += 0.5*memstress[i][j]*memstrain[i][j];
	  W += 0.5*bendstress[i][j]*bendstrain[i][j];
	}
    }
  return W;
}
  


// First variation of strains
// =============================
VarStrainData::VarStrainData()
{
  for(int i=0; i<2; i++)
    {
      var_shearstrain[i] = 0.;
      for(int j=0; j<2; j++)
	{
	  var_memstrain[i][j] = 0.;
	  var_bendstrain[i][j] = 0.;
	}
    }
}

VarStrainData::VarStrainData(const VarStrainData& Obj)
{
  for(int i=0; i<2; i++)
    {
      var_shearstrain[i] = Obj.var_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  var_memstrain[i][j]  = Obj.var_memstrain[i][j];
	  var_bendstrain[i][j] = Obj.var_bendstrain[i][j];
	}
    }
} 


VarStrainData& VarStrainData::operator=(const VarStrainData& rhs)
{
  if( this==&rhs )
    return *this;
  
  for(int i=0; i<2; i++)
    {
      var_shearstrain[i] = rhs.var_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  var_memstrain[i][j]  = rhs.var_memstrain[i][j];
	  var_bendstrain[i][j] = rhs.var_bendstrain[i][j];
	}
    }
  return *this;
}
  

bool VarStrainData::Initialize(const PointData& PD, const VarPointData& varPD)
{
  // Variation of strains
  for(int i=0; i<2; i++)
    {
      var_shearstrain[i] = 0.;
      for(int k=0; k<3; k++)
	var_shearstrain[i] += varPD.dphi[k][i]*PD.t[k] + PD.dphi[k][i]*varPD.t[k];
      
      for(int j=0; j<2; j++)
	{
	  var_memstrain[i][j] = 0.;
	  var_bendstrain[i][j] = 0.;
	  for(int k=0; k<3; k++)
	    {
	      var_memstrain[i][j] += 0.5*( varPD.dphi[k][i]*PD.dphi[k][j] + 
					   varPD.dphi[k][j]*PD.dphi[k][i] );
	      
	      var_bendstrain[i][j] += 0.5*( varPD.dphi[k][i]*PD.tprime[k][j] +
					    varPD.dphi[k][j]*PD.tprime[k][i] +
					    PD.dphi[k][i]*varPD.tprime[k][j] +
					    PD.dphi[k][j]*varPD.tprime[k][i] );
	    }
	}
    }
  return true;
}			



double VarStrainData::ComputeVarVirtualWork(const double* shearstress, 
					    const double memstress[][2], 
					    const double bendstress[][2]) const
{
  double var_W = 0.;
  for(int i=0; i<2; i++)
    {
      var_W += shearstress[i]*var_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  var_W += memstress[i][j]*var_memstrain[i][j];
	  var_W += bendstress[i][j]*var_bendstrain[i][j];
	}
    }
  return var_W;
}




// Second variation of strains
//=================================
VarVarStrainData::VarVarStrainData()
{
  for(int i=0; i<2; i++)
    {
      VAR_var_shearstrain[i] = 0.;
      for(int j=0; j<2; j++)
	{
	  VAR_var_memstrain[i][j] = 0.;
	  VAR_var_bendstrain[i][j]= 0.;
	}
    }
}


VarVarStrainData::VarVarStrainData(const VarVarStrainData& Obj)
{
  for(int i=0; i<2; i++)
    {
      VAR_var_shearstrain[i] = Obj.VAR_var_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  VAR_var_memstrain[i][j]  = Obj.VAR_var_memstrain[i][j];
	  VAR_var_bendstrain[i][j] = Obj.VAR_var_bendstrain[i][j];
	}
    }
} 



VarVarStrainData& VarVarStrainData::operator=(const VarVarStrainData& rhs)
{
  if( this==&rhs )
    return *this;
  
  for(int i=0; i<2; i++)
    {
      VAR_var_shearstrain[i] = rhs.VAR_var_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  VAR_var_memstrain[i][j]  = rhs.VAR_var_memstrain[i][j];
	  VAR_var_bendstrain[i][j] = rhs.VAR_var_bendstrain[i][j];
	}
    }
  return *this;
}

    
bool VarVarStrainData::Initialize(const PointData& PD, 
				  const VarPointData& var, const VarPointData& VAR, 
				  const VarVarPointData& VAR_var)
{
  // Second variation of strains
  for(int i=0; i<2; i++)
    {
      VAR_var_shearstrain[i] = 0.;
      for(int k=0; k<3; k++)
	VAR_var_shearstrain[i] += 
	  var.dphi[k][i]*VAR.t[k] + VAR.dphi[k][i]*var.t[k] + PD.dphi[k][i]*VAR_var.t[k];
      
      for(int j=0; j<2; j++)
	{
	  VAR_var_memstrain[i][j] = 0.;
	  VAR_var_bendstrain[i][j] = 0.;
	  for(int k=0; k<3; k++)
	    {
	      VAR_var_memstrain[i][j] += 0.5*( var.dphi[k][i]*VAR.dphi[k][j] +
					       var.dphi[k][j]*VAR.dphi[k][i] );
	      
	      VAR_var_bendstrain[i][j] += 0.5*( PD.dphi[k][i]*VAR_var.tprime[k][j] +
						PD.dphi[k][j]*VAR_var.tprime[k][i] +
						var.dphi[k][i]*VAR.tprime[k][j] +
						var.dphi[k][j]*VAR.tprime[k][i] +
						VAR.dphi[k][i]*var.tprime[k][j] +
						VAR.dphi[k][j]*var.tprime[k][i] );
	    }
	}
    }
  
  return true;
}


  
double VarVarStrainData::ComputeVarVarVirtualWork(const double* shearstress, 
						  const double memstress[][2], 
						  const double bendstress[][2], 
						  const double Ashear[][2], 
						  const double Amem[][2][2][2],
						  const double Abend[][2][2][2], 
						  const VarStrainData& varSD,
						  const VarStrainData& VARSD) const
{
  double VAR_var_W = 0.;
  
  // Material and geometric stiffness contributions
  for(int i=0; i<2; i++)
    {
      VAR_var_W += shearstress[i]*VAR_var_shearstrain[i];
      for(int j=0; j<2; j++)
	{
	  VAR_var_W += 
	    Ashear[i][j]*varSD.var_shearstrain[i]*VARSD.var_shearstrain[j] +
	    bendstress[i][j]*VAR_var_bendstrain[i][j] +
	    memstress[i][j]*VAR_var_memstrain[i][j];
	  
	  for(int k=0; k<2; k++)
	    for(int L=0; L<2; L++)
	      VAR_var_W += 
		Amem[i][j][k][L]*varSD.var_memstrain[i][j]*VARSD.var_memstrain[k][L] + 
		Abend[i][j][k][L]*varSD.var_bendstrain[i][j]*VARSD.var_bendstrain[k][L];
	}
    }
  
  return VAR_var_W;
}
