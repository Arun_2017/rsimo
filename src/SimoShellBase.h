// sriramajayam

#ifndef SIMOSHELLBASE
#define SIMOSHELLBASE

#include "ElementalOperation.h"
#include "IsotropicLinearElasticShell3D.h"
#include "SimoShellStructs.h"
#include <cstdio>
#include "gsl/gsl_sf_bessel.h"
#include <cassert>
#include <iostream>
#include <cstdio>
#include <petscvec.h>
#include <petscmat.h>
#include <LocalToGlobalMap.h>

// Additional .h files
//#include <Assembler.h>
//#include <PetscData.h>
//#include <cmath>
//#include <set>




class SimoShellBase: public DResidue
{
 public:
  
  //! Nodal positions 
  static std::vector< std::vector<double> >* Xn;
  
  //! Nodal rotations 
  static std::vector< std::vector<double> >* Rn;
  
  //! Assembles the residual and its variation
  //! The DOF array is not required since configuration updates are performed externally.
  //! \param OpArray Array of operations
  //! \param L2GMap Local to global map
  //! \param ResVec Computed residual
  //! \param DResMat Computed stiffness matrix
  //! \param APPEND defaulted to false. If true, does not zero the residual
  //! and the stiffness
  static bool Assemble(std::vector<DResidue*>& OpArray, const LocalToGlobalMap& L2GMap,
		       Vec* ResVec, Mat* DResMat,const bool APPEND=false);
  
  //! Over-writes the default assembly procedure
  //! \param OpArray Array of operations
  //! \param Dofs DOF array, not used
  //! \param L2GMap Local to global map
  //! \param ResVec Computed residual
  //! \param DResMat Computed stiffness matrix
  //! \param APPEND defaulted to false. If true, does not zero the residual
  //! and the stiffness
  static bool Assemble(std::vector<DResidue*>& OpArray, const LocalToGlobalMap& L2GMap, 
		       const Vec& Dofs, Vec* ResVec, Mat* DResMat, const bool APPEND=false);

  //! Constructor with non-trivial initial strains
  //! Assumes the all fields for midsurface deformation and rotations are 
  //! interpolated in the same way
  //! \param IElm Element for midsurface and director increment interpolation
  //! \param inodes Node numbers
  //! \param smat Material for shell
  //! \param refPhi Nodal midsurface coordinates of reference
  //! \param reft Nodal spatial directors of reference
  SimoShellBase(Element* IElm, const std::vector<int> inodes, 
		const IsotropicLinearElasticShell3D& smat, 
		const double refPhi[][3], const double reft[][3]);
  
  //! Destructor
  virtual ~SimoShellBase();
  
  //! Copy constructor
  //! \param Obj Object to be copied
  SimoShellBase(const SimoShellBase& Obj);
  
  //! Cloning
  virtual SimoShellBase* Clone() const = 0;
  
  //! Access to the element
  const Element* GetElement() const;
  
  //! Returns the node numbers for this operation
  virtual std::vector<int> GetOperationNodeNumbers() const;
  
  //! Returns the spatial director at a node 
  //! \param a Node number
  virtual std::vector<double> GetSpatialDirectorAtNode(const int a) const;
  
  //! Returns the rotation matrix at a node 
  //! \param a Node number
  virtual std::vector<double> GetRotationMatrixAtNode(const int a) const;
  
  //! Returns the midsurface position at a node
  //! \param a Node number
  virtual std::vector<double> GetMidSurfaceCoordinatesAtNode(const int a) const;
  
  //! Access to the shell material
  const IsotropicLinearElasticShell3D* GetShellMaterial() const;
  
  //! Set the shell material
  //! \param smat Object defining shell material
  virtual void SetShellMaterial(const IsotropicLinearElasticShell3D& smat);
  
  //! Access to the fields used
  virtual const std::vector<int>& GetField() const;
  
  //! Returns the number of dofs in given field
  //! \param fieldnumber Local field number (assumes standard numbering)
  virtual int GetFieldDof(const int fieldnumber) const;
  
  //! Overload the default routine since dof values are not required
  //! \param argval Dof values
  //! \param funcval Computed force vector
  virtual void GetVal(const void* argval,
		      std::vector< std::vector<double> >* funcval) const override;
  
  //! Overload the default routine since the dof values are not required
  //! \param argval Dof values
  //! \param funcval Computed force vector
  //! \param dfuncval Computed stiffness matrix
  virtual void GetDVal(const void* argval,
		       std::vector< std::vector<double> >* funcval, 
		       std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval=NULL) const override;

  
  //! Computes the force vector 
  virtual bool GetResidual(std::vector< std::vector<double> >* funcval) const = 0;
  
  //! Computes the force vector and the stiffness matrix 
  //! \param funcval Computed force vector
  //! \param dfuncval Computed stiffness matrix
  virtual bool GetJacobian
    (std::vector< std::vector<double> >* funcval, 
     std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval) const = 0;
  
  
  //! Returns the material director field, which is assumed to be a constant
  //! \param E material director
  virtual bool GetMaterialDirector(double* E) const;
  
  //! Returns the basis for material director increments
  //! \param Eperp Basis for material director increments
  virtual bool GetMaterialDirectorIncrementBasis(double Eperp[][3]) const;
 
  
  //! Maps material director dofs to spatial director dofs
  static void MapMaterialDirectorDofsToSpatialDirectorDofs
    (const double* R, const double* Tdof, double* tdof);
  
  //! Computes the exponential map 
  //! \param theta axial vector
  //! \param mat Computed matrix
  static void ExpSO3(const double* theta, double* mat);

  //! Computes the rotation matrix
  //! \param E source vector
  //! \param t Target vector
  //! \param R Computed rotation matrix
  static bool ComputeRotationMatrix(const double* E, const double* t, double* R);

  //! Consistency test
  //! \param[in] argval Configuration at which to perform the consistency test
  //! \param[in] pertEPS Tolerance to use for perturbation
  //! \param[in] pertEPS Tolerance to use for consistency check
  virtual bool ConsistencyTest(const void* argval,
			       const double pertEPS,
			       const double tolEPS) const override
  { assert(false && "SimoShellBase:: Consistency test not implemented");
    return false; }
  
 protected:
  //! Resize and initialize force and stiffness
  //! \param funcval Force vector
  //! \param dfuncval Stiffness matrix
  virtual void ResizeAndInitializeForceAndStiffness
    (std::vector< std::vector<double> >* funcval, 
     std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval) const;
  
  //! spatial dimension
  static const int SPD = 3;
  
  //! parametric dimension
  static const int PDIM = 2;
  
  //! total number of fields
  static const int nFields = 5;
  
  //! Material director
  static const double MatDir[3];
  
  //! Basis for material director increments
  static const double MatDirIncBasis[2][3];
  
  //! Element
  const Element* Elm;
  
  //! Node numbers
  std::vector<int> NodeNum;
  
  //! Material
  const IsotropicLinearElasticShell3D* SMat;
  
  //! Fields Used
  std::vector<int> FieldsUsed;
  
  //! Number of quadrature points
  int nQuad;

  //! Number of dofs
  int nDof;
  
  //! Reference strains at quadrature points
  std::vector< SimoShell::ReferenceStrains > QuadRefStrains;

};

#endif
