// Sriramajayam


#ifndef TRIANGLENOCOORDARRAY
#define TRIANGLENOCOORDARRAY

#include "Triangle.h"
#include <cassert>

//! \brief Class for triangle when no global coordinates array is provided.
/*! This class is the same as Triangle except that each triangle is provided 
 *  an element coordinates array.
 */
//! \todo I am copying the element coordinates since there is no way to return faces. Need to rectify this.

template< int spatial_dimension>
class TriangleNoCoordArray:  public Triangle<spatial_dimension>
{
 public:
  //! Constructor
  //! \param i1 connectivity of node 1.
  //! \param i2 connectivity of node 2.
  //! \param i3 connectivity of node 3.
  //! \param ielmcoord Coordinates of triangle element.
  inline TriangleNoCoordArray(int i1, int i2, int i3,
			      const double * ielmcoord)
    :Triangle<spatial_dimension>(i1, i2, i3)
    { 
      for(int a=0; a<3; a++)
	for(unsigned int k=0; k<spatial_dimension; k++)
	  elmcoord[a*spatial_dimension+k] = ielmcoord[a*spatial_dimension+k]; 
    }
  
  //! Destructor
  inline virtual ~TriangleNoCoordArray() {}

  //! Copy constructor
  //! \param NewTri Element geometry to be copied.
  inline TriangleNoCoordArray(const TriangleNoCoordArray & NewTri)
    :Triangle<spatial_dimension>(NewTri)
    { 
      for(int a=0; a<3; a++)
	for(unsigned int k=0; k<spatial_dimension; k++)
	  elmcoord[a*spatial_dimension+k] = NewTri.elmcoord[a*spatial_dimension+k];
    }
      
  //! Cloning
  inline virtual TriangleNoCoordArray * Clone() const
    { return new TriangleNoCoordArray<spatial_dimension>(*this); }

  //! Returns name of element geometry
  inline std::string GetPolytopeName() const
    { return "TriangleNoCoordArray"; }

  //! Returns geometry of requested face.
  //! \param e Face number.
  Segment<spatial_dimension> * GetFaceGeometry(int e) const
    { assert(false && "Segment geometry not available");
      return nullptr; }
    
 protected:
  //! Returns the requested coordinates of a node.
  //! \param a Node number.
  //! \param i direction number.
  virtual double GetCoordinate(int a, int i) const
    { return elmcoord[a*spatial_dimension+i]; }
  
 private:
  double elmcoord[3*spatial_dimension];
};

#endif
