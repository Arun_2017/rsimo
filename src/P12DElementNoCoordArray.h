// Sriramajayam

#ifndef P12DELEMENTNOCOORDARRAY
#define P12DELEMENTNOCOORDARRAY

#include "P12DElement.h"
#include <TriangleNoCoordArray.h>
#include "BasisFunctionsProvided.h"

//! \brief This is the class for P12DElement when there is no global coordinates array.
/*! Instead the coordinates array for each element is individually provided. However, the 
 * class still expects the connectivity numbers. Otherwise, it is identical to P12DElement.
 */

template<int NFields> 
class P12DElementNoCoordArray: public Element
{
 public:
  //! Constructor
  //! \param i1 Connectivity for node 1.
  //! \param i2 Connectivity for node 2.
  //! \param i3 Connectivity for node 3.
  //! \param ielmcoord Array containing coordinates of nodes. Should have length 6.
  inline P12DElementNoCoordArray(int i1, int i2, int i3,
				 const double * ielmcoord)
    {
      TriGeom = new TriangleNoCoordArray<2>(i1,i2,i3,ielmcoord);
      Linear<2> Shp;
      ShapesEvaluated ModelShape(Triangle_1::Bulk, &Shp, TriGeom);
      AddBasisFunctions(ModelShape);
      for(int i=0; i<NFields; ++i)
	AppendField(i);
    }
    
    //! Destructor
  inline virtual ~P12DElementNoCoordArray() 
    { delete TriGeom; }
  
  //! Copy Constructor
  //! \param NewElm Element to be copied.
  inline P12DElementNoCoordArray( const P12DElementNoCoordArray<NFields> &NewElm)
    : Element(NewElm)
  { TriGeom = NewElm.TriGeom->Clone(); }
  
  //! Cloning
  virtual P12DElementNoCoordArray * Clone() const override
    { return new P12DElementNoCoordArray(*this); }
  
  
  //! Returns the element geometry
  virtual const TriangleNoCoordArray<2> & GetElementGeometry() const override
    { return *TriGeom; }
  
 protected:
  //! Returns the position in LocalShapes where shape functions for each field is stored.
  //! \param field Field number. Range: 0 to NFields-1.
  virtual int GetFieldIndex(int field) const override
    { return 0; }
  
 private:
  TriangleNoCoordArray<2> * TriGeom;
};
  


#endif
