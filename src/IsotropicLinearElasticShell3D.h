// Sriramajayam

#ifndef ISOTROPICLINEARELASTICSHELL3D
#define ISOTROPICLINEARELASTICSHELL3D

#include "Material.h"
#include <iomanip>
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <cstdio>

class IsotropicLinearElasticShell3D
{
 public:
  //! Constructor
  //! \param E Young's modulus
  //! \param nu Poisson ratio
  //! \param h Shell thickness
  //! \param iRho Reference 3D density, assumed to be a constant
  IsotropicLinearElasticShell3D(const double E, const double nu, const double h, const double iRho = 1.);

  //! Destructor
  virtual ~IsotropicLinearElasticShell3D();
  
  //! Copy constructor
  //! \param Obj Object to be copied
  IsotropicLinearElasticShell3D(const IsotropicLinearElasticShell3D& Obj);
  
  //! Cloning
  virtual IsotropicLinearElasticShell3D* Clone() const;

  //! Returns the Young's modulus
  virtual double GetYoungModulus() const;

  //! Returns the Poisson ratio
  virtual double GetPoissonRatio() const;

  //! Returns thickness
  virtual double GetThickness() const;

  //! Returns the density of the surface in the reference configuration
  virtual double GetSurfaceDensity() const;
  
  //! Returns the moment of inertia
  virtual double GetMomentOfInertia() const;

  //! Set the moment of inertia
  virtual void SetMomentOfInertia(const double I0);
  
  //! Returns the surface density in the reference configuration
  virtual void SetSurfaceDensity(const double density);
  
  //! Set the membrane modulus
  //! \param Modulus Value of membrane modulus
  virtual void SetMembraneModulus(const double Modulus);

  //! Returns the membrane modulus
  virtual double GetMembraneModulus() const;
  
  //! Set the bending modulus
  //! \param Modulus Value of bending modulus
  virtual void SetBendingModulus(const double Modulus);
  
  //! Returns the bending modulus
  virtual double GetBendingModulus() const;

  //! Set the shear modulus
  //! \param Modulus Value of shear modulus
  virtual void SetShearModulus(const double Modulus);

  //! Returns the shear modulus
  virtual double GetShearModulus() const;

  //! Computes the strain energy for given strain measures
  //! \param memstrain Membrane strains
  //! \param shearstrain Shear strains
  //! \param bendingstrain bending strains
  //! \param W Computed strain energy density
  virtual bool ComputeStrainEnergyDensity(const double ginv[][2],
					  const double memstrain[][2], 
					  const double* shearstrain, 
					  const double bendstrain[][2], 
					  double& W) const;


  //! Computes the membrane stress resultants and modulii
  //! \param strains Membrane strain
  //! \param stress Computed membrane stress resultant 
  //! \param modulii Computed membrane stiffness modulii
  virtual bool GetMembraneConstitutiveResponse(const double ginv[][2],
					       const double strain[][2], 
					       double stress[][2],
					       double modulii[][2][2][2]=NULL) const;

  //! Computes the bending stress resultants and modulii
  //! \param strains bending strain
  //! \param stress Computed bending stress resultant 
  //! \param modulii Computed bending stiffness modulii
  virtual bool GetBendingConstitutiveResponse(const double ginv[][2],
					      const double strain[][2], 
					      double stress[][2], 
					      double modulii[][2][2][2]=NULL) const;
  
  //! Computes the shear stress resultants and modulii
  //! \param strains shear strain
  //! \param stress Computed shear stress
  //! \param modulii Computed shear stiffness modulii
  virtual bool GetShearConstitutiveResponse(const double ginv[][2],
					    const double* strain, 
					    double* stress, 
					    double modulii[][2]=NULL) const;

  
  
  //! Returns the membrane stiffness modulii
  virtual bool GetMembraneStiffness(const double ginv[][2], double modulii[][2][2][2]) const;
  
  //! Returns the bending stiffness modulii
  virtual bool GetBendingStiffness(const double ginv[][2], double modulii[][2][2][2]) const;
  
  //! Returns the shear stiffness modulii
  virtual bool GetShearStiffness(const double ginv[][2], double modulii[][2]) const;
  
 protected:
  //! Returns the H matrix
  bool GetHMatrix(const double ginv[][2], double HMatrix[][2][2][2]) const;
  
 private:
  double YoungModulus;
  double PoissonRatio;
  double thickness;
  double Rho;
  double MomentOfInertia;
  double Kmem;
  double Kbend;
  double Kshear;
 
};


#endif
