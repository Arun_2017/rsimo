// Sriramajayam

#ifndef STATICINTERPOLATEDSIMOSHELL
#define STATICINTERPOLATEDSIMOSHELL

#include "SimoShellBase.h"
#include "gsl/gsl_sf_bessel.h"


class StaticInterpolatedSimoShell: public SimoShellBase
{
 public:
  
  //! Update the midsurface and rotations
  //! \param incT Increment in material director
  static bool UpdateConfiguration(const std::vector< std::vector<double> >& incX, 
				  const std::vector< std::vector<double> >& incT);

  //! Constructor with non-trivial initial strains
  //! Assumes the all fields for midsurface deformation and rotations are 
  //! interpolated in the same way
  //! \param IElm Element for midsurface and director increment interpolation
  //! \param smat Material for shell
  //! \param refPhi Nodal midsurface coordinates of reference
  //! \param reft Nodal spatial directors of reference
  //! \param  inodes Numbering of nodes for this operation
  //! Used to access tNodal and RNodal.
  StaticInterpolatedSimoShell(Element* IElm, const std::vector<int>& inodes,
			      const IsotropicLinearElasticShell3D& smat, 
			      const double refPhi[][3], const double reft[][3]);
  
  //! Destructor
  virtual ~StaticInterpolatedSimoShell();
  
  //! Copy constructor
  //! \param Obj Object to be copied
  StaticInterpolatedSimoShell(const StaticInterpolatedSimoShell& Obj);
  
  //! Cloning
  virtual StaticInterpolatedSimoShell* Clone() const;
  
  
  //! Computes the functional being minimized
  //! \param Fval Computed functional value
  virtual bool ComputeFunctional(double& Fval) const;

  //! Computes the force vector
  //! \param funcval Computed force vector
  virtual bool GetResidual(std::vector< std::vector<double> >* funcval) const;
  
  //! Computes the force vector and stiffness matrix
  //! \param funcval Computed force vector
  //! \param dfuncval Computed stiffness matrix
  virtual bool GetJacobian(std::vector< std::vector<double> >* funcval, 
			   std::vector< std::vector< std::vector< std::vector<double> > > >* dfuncval=NULL) const;
};

#endif
