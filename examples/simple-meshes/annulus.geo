rin = 0.75;
rout = 1.25;
gap = 0.01;
Point(1) = {0, 0, 0};
Point(2) = {rin, 0, 0};
Point(3) = {0, rin, 0};
Point(4) = {-rin, 0, 0};
Point(5) = {0, -rin, 0};
Point(6) = {rin, -gap, 0.};

Circle(1) = {2, 1, 3};
Circle(2) = {3, 1, 4};
Circle(3) = {4, 1, 5};
Circle(4) = {5, 1, 6};

Point(7) = {rout, 0, 0};
Point(8) = {0, rout, 0};
Point(9) = {-rout, 0, 0};
Point(10) = {0, -rout, 0};
Point(11) = {rout, -gap, 0.};

Circle(5) = {7, 1, 8};
Circle(6) = {8, 1, 9};
Circle(7) = {9, 1, 10};
Circle(8) = {10, 1, 11};

Line(9) = {2, 7};
Line(10) = {11, 6};
Line Loop(1) = {9, 5, 6, 7, 8, 10, -4, -3, -2, -1};
Plane Surface(12) = {1};
Physical Surface(13) = {12};
Coherence;
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
Delete {
  Point{1};
}
