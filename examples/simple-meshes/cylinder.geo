Point(1) = {0, 0, 0, 0.};
Point(2) = {1, 0, 0};
Point(3) = {0, 1, 0};
Point(4) = {-1, 0, 0};
Point(5) = {0, -1, 0};
Circle(1) = {2,1,3};
Circle(2) = {3,1,4};
Circle(3) = {4,1,5};
Circle(4) = {5,1,2};
Line Loop(1) = {1,2,3,4};
Plane Surface(5) = {1};
Extrude {0, 0, 10} { Surface{5}; }
Physical Surface(28) = {22, 26, 14, 18};
Delete { Volume{1}; }
Delete { Surface{27}; }
Delete { Surface{5}; }
Transfinite Line {13} = 75 Using Progression 1;
Transfinite Line {21} = 75 Using Progression 1;
Transfinite Line {17} = 20 Using Progression 1;
Transfinite Line {12} = 20 Using Progression 1;
Transfinite Line {7} = 15 Using Progression 1;
Transfinite Line {8} = 15 Using Progression 1;
Transfinite Line {9} = 15 Using Progression 1;
Transfinite Line {10} = 15 Using Progression 1;