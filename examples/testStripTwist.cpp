// Sriramajayam

#include "P12DElementNoCoordArray.h"
#include "StaticInterpolatedSimoShell.h"
#include "MeshUtils.h"
#include "PlottingUtils.h"

//#include "TriangleSubdivision.h"

#include <Assembler.h>
#include <PetscData.h>


const int SPD = 3;
const int PDIM = 2;
const int nFields = SPD+PDIM;
const int nDof = 3;
const double Ez[] = {0.,0.,1.};

char OUTFLDR[500] = "./Plots";

using ShellType = StaticInterpolatedSimoShell;

// Compute the solution
bool ComputeEquilibriumConfiguration(std::vector<ShellType*>& OpArray,
				     const LocalToGlobalMap& L2GMap,
				     StandardAssembler<ShellType>& Asm, 
				     PetscData& PD,
				     const std::vector<int>& boundary,
				     const std::vector<double>& bvalues);

// Compute the components of the strain energy
void ComputeStrainEnergies(const std::vector<ShellType*>& OpArray,
			   double& Emem, double& Ebend, double& Eshear);


int main(int argc, char** argv)
{
  // Read 3D mesh
  CoordConn MD;
  MD.spatial_dimension = SPD;
  ReadTecplotFile((char*)"simple-meshes/10x2.msh", MD);
  const double Len = 10.;
  for(int i=0; i<0; i++)
    SubdivideTriangles(MD.connectivity, MD.coordinates, MD.nodes, MD.elements);
  char filename[500];
  sprintf(filename, "%s/ref.tec", OUTFLDR);
  PlotTecCoordConn(filename, MD);

  // Create elements
  std::vector<double> coord2D; coord2D.clear();
  for(int n=0; n<MD.nodes; n++)
    for(int i=0; i<PDIM; i++)
      coord2D.push_back( MD.coordinates[SPD*n+i] );
  Triangle<PDIM>::SetGlobalCoordinatesArray(coord2D);
  Segment<PDIM>::SetGlobalCoordinatesArray(coord2D);
  std::vector<Element*> ElmArray(MD.elements);
  for(int e=0; e<MD.elements; e++)
    ElmArray[e] = new P12DElement<nFields>(MD.connectivity[3*e],
					      MD.connectivity[3*e+1],
					      MD.connectivity[3*e+2]);
  
  // Local to global map
  StandardP12DMap L2GMap(ElmArray);

  // Create shell material
  const double YM = 1.;
  const double nu = 0.;
  const double thickness = 0.1;
  IsotropicLinearElasticShell3D SMat(YM, nu, thickness, 0.);

  // Global coordinates and rotations
  std::vector< std::vector<double> > X(MD.nodes), R(MD.nodes);
  for(int n=0; n<MD.nodes; n++)
    { X[n].resize(SPD); R[n].resize(SPD*SPD); }

  // Create operations
  ShellType::Xn = &X;
  ShellType::Rn = &R;
  std::vector<ShellType*> OpArray(MD.elements);
  for(int e=0; e<MD.elements; e++)
    {
      std::vector<int> NodeNum(nDof);
      double refPhi[nDof][SPD];
      for(int a=0; a<nDof; a++)
	{
	  NodeNum[a] = MD.connectivity[3*e+a]-1;
	  for(int i=0; i<SPD; i++)
	    refPhi[a][i] = MD.coordinates[SPD*NodeNum[a]+i];
	}
      double reft[nDof][SPD] = {{0.,0.,1.},{0.,0.,1.},{0.,0.,1.}};
      OpArray[e] = new ShellType(ElmArray[e], NodeNum, SMat,
						   refPhi, reft);
    }

  // Increment vectors
  std::vector< std::vector<double> > dX(MD.nodes), dT(MD.nodes);
  for(int n=0; n<MD.nodes; n++)
    { dX[n].resize(SPD); dT[n].resize(PDIM); }

  // Deformed configuration
  CoordConn DefMD = MD;
  double spdir[SPD*MD.nodes];
  
  // Initialize PETSc
  PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
  
  //Assembler
  StandardAssembler<ShellType> Asm(OpArray, L2GMap);
  std::vector<int> nz;
  Asm.CountNonzeros(nz);

  // Create PETSc data structure
  PetscData PD;
  //InitializePetscData(L2GMap, L2GMap.GetTotalNumDof(), PD);
  PD.Initialize(nz);

  //PCType
  PC pc;
  KSPGetPC(PD.kspSOLVER, &pc);
  PCSetType(pc,PCLU);

  
  const double dTwist = 0.25*M_PI/180.;

  // Identify nodes to clamp and node to load
  const double EPS = 1.e-4;
  std::set<int> clampNodes; clampNodes.clear();
  int centerNode = -1;
  for(int n=0; n<MD.nodes; n++)
    if( fabs(MD.coordinates[SPD*n])<EPS )
      clampNodes.insert(n);
    else if( fabs(MD.coordinates[SPD*n]-Len) + fabs(MD.coordinates[SPD*n+1])<EPS )
      centerNode = n;
  std::vector<int> boundary; 
  std::vector<double> bvalues;
  
  // Track the coordinates of the loaded node
  std::fstream pfile; pfile.open((char*)"coord.dat", std::ios::out);
  double angle = 0.;

  // Track the energies
  std::fstream efile; efile.open((char*)"energy.dat", std::ios::out);
  
  // Load stepping
  for(int step=1; step<=150; step++)
    {
      std::cout<<"\n\nLoading step "<<step<<"\n================\n";
      std::fflush( stdout );
      
      // Incorporate rotation 
      for(int n=0; n<MD.nodes; n++)
	{
	  for(int i=0; i<SPD; i++)  dX[n][i] = 0.;
	  for(int p=0; p<PDIM; p++) dT[n][p] = 0.;
	}
      dT[centerNode][1] = dTwist;
      angle += dT[centerNode][1];
      ShellType::UpdateConfiguration(dX, dT);
      
      // Boundary conditions
      boundary.clear(); bvalues.clear();
      for(std::set<int>::const_iterator it=clampNodes.begin(); it!=clampNodes.end(); it++)
	for(int f=0; f<nFields; f++)
	  { boundary.push_back( nFields*(*it)+f ); bvalues.push_back( 0. ); }
      boundary.push_back( nFields*centerNode+SPD+1 ); bvalues.push_back( 0. );
      
      // Trial solution: displace in Y
      if( 0 && step>=84 && step<=90 )
	{
	  boundary.push_back( nFields*centerNode+0 ); bvalues.push_back( 0. );
	  const double sign = (X[centerNode][1] > 0.) ? 1. : -1.;
	  for(int i=0; i<5; i++)
	    {
	      X[centerNode][1] += sign*2.5e-2;
	      std::cout<<"\nComputing trial solution with Y displacement: \n"; std::fflush( stdout );
	      if( !ComputeEquilibriumConfiguration(OpArray, L2GMap, Asm, PD, boundary, bvalues) )
		{ std::cerr<<"\nCould not compute trial configuration.\n";
		  std::fflush( stdout ); exit(1); }
	    }
	  
	  // Reset bc for equilibrium solution
	  boundary.pop_back(); bvalues.pop_back();
	}
      
      // Compute equilibrium solution
      std::cout<<"\nComputing equilibrium solution: \n"; std::fflush( stdout );
      if( !ComputeEquilibriumConfiguration(OpArray, L2GMap, Asm, PD, boundary, bvalues) )
	{ std::cerr<<"\nCould not compute equilibrium configuration.\n";
	  std::fflush( stdout ); exit(1); }
      
      // Deformed configuration
      for(int n=0; n<MD.nodes; n++)
	for(int i=0; i<SPD; i++)
	  {
	    DefMD.coordinates[SPD*n+i] = X[n][i];
	    spdir[SPD*n+i] = 0.;
	    for(int j=0; j<SPD; j++)
	      spdir[SPD*n+i] += R[n][SPD*i+j]*Ez[j];
	  }
      sprintf(filename, "%s/def-%03d.tec", OUTFLDR, step);
      PlotTecCoordConnWithNodalFields(filename, DefMD, spdir, SPD);

      // Record the coordinates of the loaded node
      pfile << angle*180./M_PI <<" "
	    <<X[centerNode][0]<<" "<<X[centerNode][1]<<" "<<X[centerNode][2]<<"\n";
      pfile.flush();

      // Energies of this configuration
      double Emem, Ebend, Eshear;
      ComputeStrainEnergies(OpArray, Emem, Ebend, Eshear);
      efile << angle*180./M_PI<<" "<<Emem<<" "<<Ebend<<" "<<Eshear<<" "<<Emem+Ebend+Eshear<<"\n";
      efile.flush();

      // Print the loaded director
      std::cout<<"\nDirector position: "<<spdir[SPD*centerNode]<<" "
	       <<spdir[SPD*centerNode+1]<<" "<<spdir[SPD*centerNode+2]<<"\n";
      std::fflush( stdout );
    }
  
  // Clean up
  efile.close();
  pfile.close();

  for(int e=0; e<MD.elements; e++)
    {
      delete ElmArray[e];
      delete OpArray[e];
    }

  PD.Destroy();
 PetscFinalize();

  
  std::cout<<"\n\n===done===\n\n"; std::fflush( stdout );
}
	



// Compute the solution
bool ComputeEquilibriumConfiguration(std::vector<ShellType*>& OpArray,
				     const LocalToGlobalMap& L2GMap,
				     StandardAssembler<ShellType>& Asm,
				     PetscData& PD,
				     const std::vector<int>& boundary,
				     const std::vector<double>& bvalues)
{
  // Solve for equilibrium configuration
  int iter = 0;
  const int nNodes = int(ShellType::Xn->size());

   // Increment vectors
  std::vector< std::vector<double> > dX(nNodes), dT(nNodes);
  for(int n=0; n<nNodes; n++)
    { dX[n].resize(SPD); dT[n].resize(PDIM); }
  
  while(true)
    {
      std::cout<<"Iteration "<<++iter<<": "; std::fflush( stdout );
      
      // Assemble matrix-vector system
      VecSet(PD.resVEC, 0.);
      VecSet(PD.solutionVEC, 0.);

      //      Asm.Assemble
      Asm.Assemble(nullptr, PD.resVEC, PD.stiffnessMAT);
      
      // Set Dirichlet BCs
      PD.SetDirichletBCs(boundary, bvalues);
      
      // Solve
      PD.Solve();
      
      // Check convergence
      if( PD.HasConverged( 1.e-10, 1., 1.) )
	{ std::cout<<"\nconverged!\n"; std::fflush( stdout ); break; }
      
      // Get configuration increments
      VecScale(PD.solutionVEC, -1.);
      for(int n=0; n<nNodes; n++)
	{
	  int msindex[] = {nFields*n, nFields*n+1, nFields*n+2};
	  int tindex[] =  {nFields*n+SPD, nFields*n+SPD+1};
	  VecGetValues(PD.solutionVEC, SPD, msindex, &dX[n][0]);
	  VecGetValues(PD.solutionVEC, PDIM, tindex, &dT[n][0]);
	}
      ShellType::UpdateConfiguration(dX, dT);
    }
  return true;
}



// Compute the components of the strain energy
void ComputeStrainEnergies(const std::vector<ShellType*>& OpArray,
			   double& Emem, double& Ebend, double& Eshear)
{
  Emem = 0.;
  Eshear = 0.;
  Ebend = 0.;
  
  // Get the material
  const IsotropicLinearElasticShell3D* SMat =
    dynamic_cast<const ShellType*>(OpArray[0])->GetShellMaterial();

  // Create materials to evaluate components of strain energy
  IsotropicLinearElasticShell3D* MemMat = SMat->Clone();
  MemMat->SetBendingModulus(0.);
  MemMat->SetShearModulus(0.);

  IsotropicLinearElasticShell3D* BendMat = SMat->Clone();
  BendMat->SetMembraneModulus(0.);
  BendMat->SetShearModulus(0.);

  IsotropicLinearElasticShell3D* ShearMat = SMat->Clone();
  ShearMat->SetMembraneModulus(0.);
  ShearMat->SetBendingModulus(0.);

  const int nElements = int(OpArray.size());
  for(int e=0; e<nElements; e++)
    {
      const ShellType* Op =
	dynamic_cast<const ShellType*>(OpArray[e]);
      if( Op==NULL )
	{ std::cerr<<"\nComputeStrainEnergies()- Could not typecast opetation.\n";
	  std::fflush( stdout ); exit(1); }

      ShellType* Copy = Op->Clone();

      // Contributions
      Copy->SetShellMaterial(*MemMat);
      double dMem = 0.;
      Copy->ComputeFunctional(dMem);
      Emem += dMem;

      Copy->SetShellMaterial(*BendMat);
      double dBend = 0.;
      Copy->ComputeFunctional(dBend);
      Ebend += dBend;

      Copy->SetShellMaterial(*ShearMat);
      double dShear = 0.;
      Copy->ComputeFunctional(dShear);
      Eshear += dShear;

      double dW = 0.;
      Op->ComputeFunctional(dW);
      
      // Consistency check
      if( fabs(dBend+dShear+dMem-dW)>1.e-12 )
	{ std::cerr<<"\nComputeStrainEnergies()- "
		   <<"Inconsistency in computed components of strain energies.\n";
	  std::fflush( stdout ); exit(1); }
      
      delete Copy;
    }
  delete BendMat;
  delete ShearMat;
  delete MemMat;
}
